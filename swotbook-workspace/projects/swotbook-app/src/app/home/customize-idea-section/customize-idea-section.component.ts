import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-customize-idea-section',
  templateUrl: './customize-idea-section.component.html',
  styleUrls: ['./customize-idea-section.component.scss']
})
export class CustomizeIdeaSectionComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;

  constructor() { }

  ngOnInit() {
  }

}
