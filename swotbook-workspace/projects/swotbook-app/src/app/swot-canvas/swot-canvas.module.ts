import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwotCanvasRoutingModule } from './swot-canvas-routing.module';
import { SwotCanvasMainComponent } from './swot-canvas-main/swot-canvas-main.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  swotCanvasInitialState,
  swotCanvasReducer
} from './+state/swot-canvas.reducer';
import { SwotCanvasEffects } from './+state/swot-canvas.effects';
import { SwotCardComponent } from './components/swot-card/swot-card.component';
import { TimeStampToDatePipe } from '../pipes/time-stamp-to-date.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectSettingDialogComponent } from './dialogs/project-setting-dialog/project-setting-dialog.component';
import { CustomSnackbarComponent } from './custom-snackbar/custom-snackbar.component';
import { ColorPickerModule } from 'ngx-color-picker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatChipsModule} from '@angular/material/chips';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  imports: [
    CommonModule,
    SwotCanvasRoutingModule,
    StoreModule.forFeature('swotCanvas', swotCanvasReducer, {
      initialState: swotCanvasInitialState
    }),
    EffectsModule.forFeature([SwotCanvasEffects]),
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatChipsModule,
    MatToolbarModule,
    MatStepperModule,
    MatDividerModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatCheckboxModule,
    FormsModule,
    MatTooltipModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    ColorPickerModule,
    MatSlideToggleModule
  ],
  declarations: [
    SwotCanvasMainComponent,
    SwotCardComponent,
    TimeStampToDatePipe,
    ProjectSettingDialogComponent,
    CustomSnackbarComponent
  ],
  providers: [SwotCanvasEffects],
  entryComponents: [
    ProjectSettingDialogComponent,
    CustomSnackbarComponent
  ],
})
export class SwotCanvasModule {}
