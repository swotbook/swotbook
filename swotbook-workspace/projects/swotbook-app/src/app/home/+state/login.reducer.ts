import {LoginActions, LoginActionTypes} from './login.actions';
import {AppUserInfo, CurrentUser, OmiseSchedule, SelectedPlan} from './login.model';

export interface Login {
  currentUser: CurrentUser;
  appUserInfo: AppUserInfo;
  isLoading: boolean;
  selectedPlan: SelectedPlan;
  userCreditCardList: any[];
  userSubType: 'std' | 'prm' | 'pro';
  userChargeSchedule: OmiseSchedule;
  authRedirectPage: string;
  userDaysUntilSubEnd:number;
}

// Subscription logic
// get active charge schedule id from appUserInfo;
//    if {empty}{user has NoSubscriptionActive()};
//    else if {has id}{get schedule id}
//        if (charge schedule is schedule.active) {
//            user is active, get plan from charge description in schedule
//            check occurrence
//            if(has occurrence){
//                able to show cancel subscription button
//            }else(has no occurrence){
//                hide cancel subscription button // abuse sub spamming of free use.
//            }
//        } else if (not active) {
//            check occurrence of latest charge
//            if(no occurrence){user never been charged, user has NoSubscriptionActive()}
//            else{
//               user has been charged calculate day left;
//               mark user as userCanceled$
//               calculate day left to display
//            }
//         }

export const loginInitialState: Login = {
  currentUser: {
    displayName: '',
    email: '',
    phoneNumber: '',
    photoURL: '',
    providerId: '',
    uid: '',
  },

  userChargeSchedule: {
    object: '',
    id: '',
    status: '',
    every: 0,
    period: '',
    on: {},
    in_words: '',
    start_date: '',
    end_date: '',
    occurrences: {
      object: '',
      offset: 0,
      limit: 0,
      from: '',
      to: '',
      order: '',
      total: 0,
      data: [],
    },
    next_occurrence_dates: [],
    deleted: false
  },

  appUserInfo: {
    omiseId: '',
    activeChargeScheduleId: '',
    lastDateAvailable: '',
    lastOccurrence: ''
  },

  userSubType: 'std',
  userCreditCardList: [],

  selectedPlan: {
    isSelected: false,
    subType: 'std',
    period: 'monthly',
    currency:'usd'
  },
  isLoading: false,
  authRedirectPage: '/swot',
  userDaysUntilSubEnd:0
};

export interface LoginState {
  readonly login: Login;
}

export function loginReducer(
  state = loginInitialState,
  action: LoginActions
): Login {
  switch (action.type) {

    case LoginActionTypes.GET_USER: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.GOOGLE_LOGIN: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.FACEBOOK_LOGIN: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_GET_CREDIT_CARD: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_ATTACH_CREDIT_CARD: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_DELETE_CARD: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_DELETE_CARD_SUCCESS: {
      return {...state, isLoading: false};
    }

    case LoginActionTypes.OMISE_SCHEDULED_CHARGE: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_SCHEDULED_CHARGE_SUCCESS: {
      return {...state, isLoading: false};
    }

    case LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE: {
      return {...state, isLoading: true};
    }

    case LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        userChargeSchedule: action.schedule
      };
    }

    case LoginActionTypes.USER_AUTHENTICATED: {

      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          uid: action.userData.uid,
          displayName: action.userData.displayName,
          email: action.userData.email,
          phoneNumber: action.userData.phoneNumber,
          photoURL: action.userData.photoURL,
          providerId: action.userData.providerId
        },
        isLoading: false
      };
    }

    case LoginActionTypes.USER_NOT_AUTHENTICATED: {
      return {
        ...state,
        isLoading: false,
        currentUser: loginInitialState.currentUser,
        appUserInfo: loginInitialState.appUserInfo
      };
    }

    case LoginActionTypes.GET_USER_INFO_SUCCESS: {
      return {...state, appUserInfo: action.appUserInfo};
    }

    case LoginActionTypes.OMISE_GET_CREDIT_CARD_SUCCESS: {
      return {
        ...state,
        appUserInfo: {...state.appUserInfo},
        userCreditCardList: action.creditCardList,
        isLoading: false
      };
    }

    case LoginActionTypes.OMISE_ATTACH_CREDIT_CARD_SUCCESS: {
      return {
        ...state,
        appUserInfo: {...state.appUserInfo},
        userCreditCardList: action.creditCardList,
        isLoading: false
      };
    }

    case LoginActionTypes.SELECT_PLAN: {
      return {
        ...state,
        selectedPlan: action.selectedPlan,
      };
    }

    case LoginActionTypes.OMISE_GET_SCHEDULED_CHARGE: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case LoginActionTypes.NO_SUBSCRIPTION_ACTIVE: {
      return {...state,
        isLoading: false,
        userSubType:'std'
      };
    }

    case LoginActionTypes.CHECK_SUBSCRIPTION_AVAILABILITY: {
      return {
        ...state,
        isLoading: false,
        userChargeSchedule: action.schedule
      };
    }

    case LoginActionTypes.SUBSCRIPTION_ACTIVE: {
      return {
        ...state,
        userSubType: action.subType,
        userDaysUntilSubEnd: action.userDaysUntilSubEnd
      };
    }

    case LoginActionTypes.SET_AUTH_REDIRECT: {
      return {
        ...state,
        authRedirectPage: action.authRedirectPage
      };
    }

    case LoginActionTypes.OMISE_REFUND_CHARGE:{
      return {
        ...state,
        isLoading:true
      };
    }

    case LoginActionTypes.OMISE_REFUND_CHARGE_SUCCESS:{
      return {
        ...state,
        isLoading:false,
        userDaysUntilSubEnd:0
      };
    }

    default:
      return state;
  }
}
