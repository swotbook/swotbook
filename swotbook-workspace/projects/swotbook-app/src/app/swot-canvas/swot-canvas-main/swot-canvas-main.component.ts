import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { swotCanvasInitialState, SwotCanvasState } from '../+state/swot-canvas.reducer';
import { Observable } from 'rxjs';
import {
  SwotCanvasModel,
  SwotCard,
  SwotProject
} from '../+state/swot-canvas.model';
import {
  ProjectSub,
  CanvasAdd,
  CanvasInProjectSub,
  CanvasSelect,
  CardAdd,
  CardsInCanvasSub,
  CardMove,
  ProjectAdd,
  ProjectSelect,
  ProjectDelete,
  ProjectMove,
  CanvasDelete,
  CanvasModify, ProjectModify, CanvasCopy, CardBatchDelete
} from '../+state/swot-canvas.actions';
import { LoginState } from '../../home/+state/login.reducer';
import { AppUtilService } from '../../app.util.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import * as Sortable from '@shopify/draggable/lib/sortable';
import { take } from 'rxjs/operators';
import { LogOut } from '../../home/+state/login.actions';
import 'rxjs-compat/add/operator/take';
import {CurrentVersion} from '../../app.version';

import * as firebase from 'firebase/app';
import { MatDialog } from '@angular/material/dialog';
import { ProjectSettingDialogComponent } from '../dialogs/project-setting-dialog/project-setting-dialog.component';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-swot-canvas-main',
  templateUrl: './swot-canvas-main.component.html',
  styleUrls: ['./swot-canvas-main.component.css'],
  animations: [
    trigger('leftSideDrawerState', [
      state('show', style({
        transform: 'translateX(0)',
      })),
      state('hide',   style({
        transform: 'translateX(-110%)'

      })),
      transition('show => hide', animate('500ms ease-in')),
      transition('hide => show', animate('500ms ease-out'))
    ]),
    trigger('rightSideDrawerState', [
      state('show', style({
        transform: 'translateX(0)',
        // display: 'absolute'
      })),
      state('hide',   style({
        transform: 'translateX(100%)',
        // display: 'none'

      })),

      transition('show => hide', animate('500ms ease-in')),
      transition('hide => show', animate('500ms ease-out'))
    ]),
    trigger('canvasZoomState', [
      state('zoomOut', style({

        transformOrigin:'top left',
        transform:'scale(0.5)',
        width:'190vw'

      })),
      state('zoomIn',   style({

      })),

      transition('zoomIn => zoomOut', animate('500ms ease-in')),
      transition('zoomOut => zoomIn', animate('500ms ease-out'))
    ])
  ]
})
export class SwotCanvasMainComponent implements OnInit, OnDestroy {
  sortable: any;
  sortTimeline: any;
  allProject$: Observable<SwotProject[]>;
  activeProject$: Observable<SwotProject>;
  activeProject: SwotProject;
  activeProjectId$: Observable<string>;
  allCanvas$: Observable<SwotCanvasModel[]>;
  activeCanvas$: Observable<SwotCanvasModel>;
  activeCanvas: SwotCanvasModel;
  cardListN$: Observable<SwotCard[]>;
  cardListS$: Observable<SwotCard[]>;
  cardListW$: Observable<SwotCard[]>;
  cardListO$: Observable<SwotCard[]>;
  cardListT$: Observable<SwotCard[]>;
  cardListALL$: Observable<SwotCard[]>;
  currentUser$: Observable<firebase.UserInfo>;
  currentUserId:string;
  loading$: Observable<boolean>;
  userSubType$: Observable<string>;
  userSubType:string;

  projectDisplayLimit = 100;
  canvasDisplayLimit = 100;

  hideLeftDrawer = true;
  hideRightDrawer = true;
  isZoomOut = false;
  isMobile = false;

  currentVersion = CurrentVersion;

  constructor(
    private store: Store<SwotCanvasState>,
    private loginStore: Store<LoginState>,
    public util: AppUtilService,
    public dialog: MatDialog,
  ) {

    this.loading$ = this.store.select(state => state.swotCanvas.loading);

    this.currentUser$ = this.loginStore.select(
      state => state.login.currentUser
    );
    this.currentUser$.pipe(untilDestroyed(this)).subscribe(currentUser => {
      if (currentUser.uid) {
        this.currentUserId = currentUser.uid;
        this.store.dispatch(new ProjectSub(currentUser.uid));
      }
    });

    this.allProject$ = this.store.select(
      state => state.swotCanvas.allTimeline
    );

    this.activeProject$ = this.store.select(
      state => state.swotCanvas.activeProject
    );
    this.activeProject$
      .pipe(untilDestroyed(this))
      .subscribe(project => {
        this.activeProject = project;
      });


    this.activeProjectId$ = this.store.select(state => state.swotCanvas.activeProject.id);
    this.activeProjectId$.pipe(untilDestroyed(this)).subscribe(id => {
      this.hideRightDrawer = this.activeProject.defaultHideTimeline && true;

    });

    this.allCanvas$ = this.store.select(state => state.swotCanvas.allCanvas);

    this.activeCanvas$ = this.store.select(
      state => state.swotCanvas.activeCanvas
    );
    this.activeCanvas$.pipe(untilDestroyed(this)).subscribe(canvas => {
      this.activeCanvas = canvas;
    });

    this.cardListN$ = this.store.select(state => state.swotCanvas.cardListN);
    this.cardListS$ = this.store.select(state => state.swotCanvas.cardListS);
    this.cardListW$ = this.store.select(state => state.swotCanvas.cardListW);
    this.cardListO$ = this.store.select(state => state.swotCanvas.cardListO);
    this.cardListT$ = this.store.select(state => state.swotCanvas.cardListT);
    this.cardListALL$ = this.store.select(
      state => state.swotCanvas.cardListAll
    );

    this.userSubType$ = this.loginStore.select(state => state.login.userSubType);
    this.userSubType$.pipe(untilDestroyed(this)).subscribe((a) => {

        this.userSubType = a;

        if(this.userSubType === 'std'){

          this.projectDisplayLimit = 3;
          this.canvasDisplayLimit = 6;

        }else if(this.userSubType === 'prm'){

          this.projectDisplayLimit = 10;
          this.canvasDisplayLimit = 20;
        }

        // this.store.dispatch(new ProjectSub(this.currentUserId));

        // this.store.dispatch(new CanvasInProjectSub(this.activeCanvas.id,this.currentUserId));

    });

  }

  ngOnInit() {

    if(this.util.detectMobile()){
      this.isMobile = true;
      this.isZoomOut = true;
    }

    this.sortTimeline = new Sortable(
      document.querySelectorAll('.projects-container'),
      {
        draggable: '.project-list',
        delay: '200'
      }
    );

    this.sortTimeline.on('sortable:start', evt => {
    });
    this.sortTimeline.on('sortable:sort', evt => {
    });
    this.sortTimeline.on('sortable:sorted', evt => {
    });
    this.sortTimeline.on('sortable:stop', evt => {
      this.allProject$.pipe(take(1)).subscribe(allTimeline => {
        const movedTimeline = allTimeline.find(
          timeline => timeline.id === evt.data.dragEvent.data.source.id
        );

        this.store.dispatch(
          new ProjectMove(movedTimeline, evt.data.oldIndex, evt.data.newIndex)
        );
      });
    });


    this.sortable = new Sortable(document.querySelectorAll('.card-container'), {
      draggable: 'app-swot-card',
      delay: '200'

      // scrollable: {
      //   speed: 10,
      //   sensitivity: 30,
      //   scrollableElements: document.getElementById('snap-container')
      //
      // }
    });

    // TODO scroll when drag item to edge of screen.
    // this.sortable.removePlugin(Draggable.Plugins.Scrollable);

    this.sortable.on('sortable:start', evt => {
    });
    this.sortable.on('sortable:sort', evt => {
    });
    this.sortable.on('sortable:sorted', evt => {
    });
    this.sortable.on('sortable:stop', evt => {
      this.cardListALL$.pipe(take(1)).subscribe(allCardList => {
        const movedCard = allCardList.find(
          card => card.id === evt.data.dragEvent.data.source.id
        );
        this.store.dispatch(
          new CardMove(
            movedCard,
            evt.data.oldContainer.id,
            evt.data.newContainer.id,
            evt.data.oldIndex,
            evt.data.newIndex
          )
        );
      });
    });
  }

  ngOnDestroy() {
  }

  calculateSummary(cardList) {
    let summary = 0;
    for (const card of cardList) {
      summary = summary + card.score * card.weight;
    }
    return summary;
  }

  addCard(container: 'S' | 'W' | 'O' | 'T' | 'N') {
    let allCard: SwotCard[];
    this.cardListALL$.take(1).subscribe(a => (allCard = a));

    if (allCard.length <= 100) {
      this.currentUser$.take(1).subscribe(currentUser => {
        this.activeCanvas$.take(1).subscribe(canvas => {
          this[`cardList${container}$`].pipe(take(1)).subscribe(cardList => {
            const newCard: SwotCard = {
              createdBy: currentUser.uid,
              canvasId: canvas.id,
              createdAt: this.util.getTimestampNow(),
              title: '',
              containIn: container,
              prevContainIn: container,
              updatedAt: this.util.getTimestampNow(),
              score: 0,
              weight: 1,
              index: cardList.length,
              lastModified: '',
              isChecked: false
            };

            this.store.dispatch(new CardAdd(newCard, canvas));
          });
        });
      });
    } else {
      alert('Cannot create more than 100 cards.');
    }
  }

  selectCanvas(canvas: SwotCanvasModel) {
    this.store.dispatch(new CanvasSelect(canvas));
    this.store.dispatch(new CardsInCanvasSub(canvas));
  }

  addProject() {
    let allTimeline: SwotProject[];

    this.allProject$.take(1).subscribe(a => (allTimeline = a));

    if (allTimeline.length <= 50) {
      this.currentUser$.take(1).subscribe(currentUser => {
        const newTimeline: SwotProject = {
          createdBy: currentUser.uid,
          index: allTimeline.length,
          title: 'Your new project',
          description: 'You cool project description',
          createdAt: this.util.getTimestampNow(),
          updatedAt: this.util.getTimestampNow(),
          row1Name: 'Internal',
          row2Name: 'External',
          col1Name: 'Positive',
          col2Name: 'Negative',
          row1col1Name: 'Strengths',
          row1col2Name: 'Weaknesses',
          row2col1Name: 'Opportunities',
          row2col2Name: 'Threats',
          isShowLabel: true,
          isTodo: true,
          row1col1Color: swotCanvasInitialState.activeProject.row1col1Color,
          row2col1Color: swotCanvasInitialState.activeProject.row2col1Color,
          row1col2Color: swotCanvasInitialState.activeProject.row1col2Color,
          row2col2Color: swotCanvasInitialState.activeProject.row2col2Color,
          defaultHideTimeline: swotCanvasInitialState.activeProject.defaultHideTimeline
        };

        this.store.dispatch(new ProjectAdd(newTimeline));
      });
    } else {
      alert('Cannot create more than 50 projects');
    }
  }

  selectProject(timeline: SwotProject) {
    let userId: string;

    this.currentUser$.take(1).subscribe(a => (userId = a.uid));

    this.store.dispatch(new ProjectSelect(timeline));
    this.store.dispatch(new CanvasInProjectSub(timeline.id, userId));
  }

  addCanvas() {
    let userId: string;
    let activeTimeline: SwotProject;
    let allCanvas: SwotCanvasModel[];

    this.currentUser$.take(1).subscribe(a => (userId = a.uid));
    this.activeProject$.take(1).subscribe(a => (activeTimeline = a));
    this.allCanvas$.take(1).subscribe(a => (allCanvas = a));

    if (allCanvas.length <= 100) {


      const canvas: SwotCanvasModel = {
        inTimeline: activeTimeline.id,
        createdBy: userId,
        createdAt: this.util.getTimestampNow(),
        updatedAt: this.util.getTimestampNow(),
        title: `New Canvas`,
        analysis: ''
      };

      this.store.dispatch(new CanvasAdd(canvas));
    } else {
      alert('Cannot create more than 100 canvas.');
    }
  }

  deleteCanvas(canvas: SwotCanvasModel) {

    let allCanvas: SwotCanvasModel[];

    this.allCanvas$.take(1).subscribe(a => allCanvas = a);

    if (allCanvas.length === 1) {

      alert('At least one canvas must be active');

    } else {

      this.store.dispatch(new CanvasDelete(canvas));

    }


  }

  logOut() {
    this.loginStore.dispatch(new LogOut());
  }

  isActiveTimeline(timelineId: string) {
    if (this.activeProject.id === timelineId) {
      return true;
    }
  }

  isActiveCanvas(canvasId: string) {
    if (this.activeCanvas.id === canvasId) {
      return true;
    }
  }

  isActiveCanvasIcon(canvasId: string) {
    if (this.activeCanvas.id === canvasId) {
      return '../../../assets/icons/timeline_filled.svg';
    } else {
      return '../../../assets/icons/timeline_outlined.svg';
    }
  }

  deleteTimeline(timeline: SwotProject) {
    this.store.dispatch(new ProjectDelete(timeline));
  }

  modifyCanvas(canvas: SwotCanvasModel) {
    const canvasTitle = (<HTMLInputElement>(
      document.getElementById('canvas-title')
    )).value;

    const canvasAnalysis = (<HTMLInputElement>(
      document.getElementById('canvas-analysis')
    )).value;

    const newCanvas: SwotCanvasModel = {
      ...canvas,
      title: canvasTitle,
      analysis: canvasAnalysis
    };

    this.store.dispatch(new CanvasModify(newCanvas));
  }

  openProjectSetting() {

    const dialogRef = this.dialog.open(ProjectSettingDialogComponent, {
      height: 'auto',
      width: '95%',
      maxWidth: '600px'
    });

    dialogRef.afterClosed().subscribe(() => {

    });

  }

  toggleDefaultOpen() {

    const updatedProject: SwotProject = {
      ...this.activeProject,
      defaultHideTimeline: !this.activeProject.defaultHideTimeline
    };

    this.store.dispatch(new ProjectModify(updatedProject));

  }

  saveAsPDF(){

  //
  //   console.log('saving to pdf');

  // let doc = new jsPDF();
  // doc.addHTML(document.getElementById("main-container"),()=>{
  //   doc.save('swot.pdf');
  // });
  //
  // var pdf = new jsPDF();
  //
  // pdf.html(document.getElementById("main-container"));
  //
  // pdf.save('swot.pdf')
  //
  // html2canvas(document.getElementById('S'),{
  //
  //
  // }).then(function(canvas) {
  //   document.body.appendChild(canvas);
  // });
  //
  // let html_container = document.getElementById('main-container');;
  // let html = html_container.outerHTML;
  //
  // let canvas = <HTMLCanvasElement>document.createElement("CANVAS",);
  //
  // rasterizeHTML.drawHTML(html, canvas).then((result)=>{
  //   document.body.appendChild(result.image);
  // });

  }

  copyCanvas(copyType: 'ALL' | 'UNCHECKED') {

    let allCard: SwotCard[];
    let copiedCards: SwotCard[];
    let removedCards: SwotCard[] = [];

    this.cardListALL$.pipe(take(1)).subscribe(a => allCard = a);

    const newCanvas: SwotCanvasModel = {
      inTimeline: this.activeProject.id,
      createdBy: this.activeCanvas.createdBy,
      createdAt: this.util.getTimestampNow(),
      updatedAt: this.util.getTimestampNow(),
      title: `Copy of ${this.activeCanvas.title}`,
      analysis: this.activeCanvas.analysis
    };

    if (copyType === 'UNCHECKED') {

      copiedCards = allCard.filter(card => !card.isChecked);
      removedCards = allCard.filter(card => card.isChecked === false);

      this.store.dispatch(new CanvasCopy(newCanvas, copiedCards));
      this.store.dispatch((new CardBatchDelete(this.activeCanvas, removedCards)));

    } else {

      this.store.dispatch(new CanvasCopy(newCanvas, allCard));

    }

  }

  getLeftDrawerState(){
    return this.hideLeftDrawer? 'hide' : 'show';
  }

  getRightSideDrawerState(){
    return (this.hideRightDrawer || !this.activeProject.id)? 'hide' : 'show';

  }

  getCanvasZoomState(){
    return this.isZoomOut? 'zoomOut':'zoomIn';
  }


  displayUserSubType():string{
    if(this.userSubType === 'prm' || this.userSubType === 'pro'){

      if(this.userSubType === 'prm'){
        return 'Premium user';
      }else if(this.userSubType === 'pro'){
        return 'Pro user';
      }

    }else{
      return '';
    }
  }

  getBackgroundColor(colorFieldName:string){

    if(this.userSubType ==='std'){
      return '#f5f5f5';
      // return this.activeProject[colorFieldName];

    }else{
      return this.activeProject[colorFieldName];
    }

  }

}
