import { Component, Input, OnInit } from '@angular/core';
import { SwotCard, SwotProject } from '../../+state/swot-canvas.model';
import { AppUtilService } from '../../../app.util.service';
import { Store } from '@ngrx/store';
import { SwotCanvasState } from '../../+state/swot-canvas.reducer';
import { CardAdd, CardDelete, CardModify } from '../../+state/swot-canvas.actions';

@Component({
  selector: 'app-swot-card',
  templateUrl: './swot-card.component.html',
  styleUrls: ['./swot-card.component.css']
})
export class SwotCardComponent implements OnInit {
  @Input() card: SwotCard;
  @Input() project: SwotProject;

  constructor(
    public util: AppUtilService,
    public store: Store<SwotCanvasState>
  ) {
  }

  ngOnInit() {
  }

  modifyCard(oldCard: SwotCard) {
    const titleInput = (<HTMLInputElement>(
      document.getElementById(oldCard.id + 'title')
    )).value;


    const newCardInput: SwotCard = {
      ...oldCard,
      title: titleInput,
      updatedAt: this.util.getTimestampNow()
    };

    this.store.dispatch(new CardModify(oldCard, newCardInput));
  }

  deleteCard(swotCard: SwotCard) {
    this.store.dispatch(new CardDelete(swotCard));
  }

  onKeydown(event) {

    if (event.key === 'Enter') {

      const newCard: SwotCard = {
        title: '',
        index: this.card.index + 1,
        containIn: this.card.containIn,
        prevContainIn: this.card.prevContainIn,
        lastModified: '',
        updatedAt: this.util.getTimestampNow(),
        createdAt: this.util.getTimestampNow(),
        createdBy: this.card.createdBy, // user id
        canvasId: this.card.canvasId, // canvas id
        score: 0,
        weight: 0,
        isChecked: false
      };

      const currentCanvas$ = this.store.select(state => state.swotCanvas.activeCanvas);
      currentCanvas$.take(1).subscribe((currentCanvas) => {
        this.store.dispatch(new CardAdd(newCard, currentCanvas));
      });

    }


  }
}

