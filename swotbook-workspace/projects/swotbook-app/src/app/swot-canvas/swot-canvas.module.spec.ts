import { SwotCanvasModule } from './swot-canvas.module';

describe('SwotCanvasModule', () => {
  let swotCanvasModule: SwotCanvasModule;

  beforeEach(() => {
    swotCanvasModule = new SwotCanvasModule();
  });

  it('should create an instance', () => {
    expect(swotCanvasModule).toBeTruthy();
  });
});
