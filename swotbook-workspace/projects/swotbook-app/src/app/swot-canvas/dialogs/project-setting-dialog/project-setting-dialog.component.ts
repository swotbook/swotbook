import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SwotCanvasState } from '../../+state/swot-canvas.reducer';
import { Store } from '@ngrx/store';
import { SwotProject } from '../../+state/swot-canvas.model';
import { ProjectModify } from '../../+state/swot-canvas.actions';
import { Observable } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import {LoginState} from '../../../home/+state/login.reducer';
import {untilDestroyed} from 'ngx-take-until-destroy';

@Component({
  selector: 'app-project-setting-dialog',
  templateUrl: './project-setting-dialog.component.html',
  styleUrls: ['./project-setting-dialog.component.css']
})
export class ProjectSettingDialogComponent implements OnInit, OnDestroy {

  projectSettingForm: FormGroup;

  activeProject$: Observable<SwotProject>;

  userSubType$: Observable<string>;
  userSubType:string;


  public arrayColors: any = {

    color1: '',
    color2: '',
    color3: '',
    color4: '',
  };

  public selectedColor = 'color1';


  constructor(
    public dialog: MatDialogRef<ProjectSettingDialogComponent>,
    private swotStore: Store<SwotCanvasState>,
    private loginStore: Store<LoginState>
  ) {

    this.activeProject$ = this.swotStore.select(state => state.swotCanvas.activeProject);
    this.userSubType$ = this.loginStore.select(state=>state.login.userSubType);
    this.userSubType$.pipe(untilDestroyed(this)).subscribe(a => this.userSubType = a);
  }

  ngOnInit() {

    this.createForm();

  }

  ngOnDestroy(): void {
  }


  createForm() {

    this.activeProject$.take(1).subscribe((activeProject) => {

      this.projectSettingForm = new FormGroup({
        title: new FormControl(activeProject.title),
        description: new FormControl(activeProject.description ),
        row1Name: new FormControl(activeProject.row1Name),
        row2Name: new FormControl(activeProject.row2Name),
        col1Name: new FormControl(activeProject.col1Name),
        col2Name: new FormControl(activeProject.col2Name),
        row1Color: new FormControl(''),
        row2Color: new FormControl(''),
        col1Color: new FormControl(''),
        col2Color: new FormControl(''),
        row1col1Name: new FormControl(activeProject.row1col1Name),
        row1col2Name: new FormControl(activeProject.row1col2Name),
        row2col1Name: new FormControl(activeProject.row2col1Name),
        row2col2Name: new FormControl(activeProject.row2col2Name),
        isShowLabel: new FormControl(activeProject.isShowLabel),
        isTodo: new FormControl(activeProject.isTodo)

      });

      this.arrayColors.color1 = activeProject.row1col1Color || '#f5f5f5';
      this.arrayColors.color2 = activeProject.row1col2Color || '#f5f5f5';
      this.arrayColors.color3 = activeProject.row2col1Color || '#f5f5f5';
      this.arrayColors.color4 = activeProject.row2col2Color || '#f5f5f5';

    });

  }

  modifyProject() {

    this.activeProject$.take(1).subscribe((activeProject) => {
      const updatedProject: SwotProject = {
        ...activeProject,
        title: this.projectSettingForm.get('title').value,
        description: this.projectSettingForm.get('description').value,
        row1Name: this.projectSettingForm.get('row1Name').value,
        row2Name: this.projectSettingForm.get('row2Name').value,
        col1Name: this.projectSettingForm.get('col1Name').value,
        col2Name: this.projectSettingForm.get('col2Name').value,
        row1col1Name: this.projectSettingForm.get('row1col1Name').value,
        row1col2Name: this.projectSettingForm.get('row1col2Name').value,
        row2col1Name: this.projectSettingForm.get('row2col1Name').value,
        row2col2Name: this.projectSettingForm.get('row2col2Name').value,
        isShowLabel: this.projectSettingForm.get('isShowLabel').value,
        isTodo: this.projectSettingForm.get('isTodo').value,
        row1col1Color: this.arrayColors.color1,
        row1col2Color: this.arrayColors.color2,
        row2col1Color: this.arrayColors.color3,
        row2col2Color: this.arrayColors.color4

      };

      this.swotStore.dispatch(new ProjectModify(updatedProject));

      this.dialog.close();
    });

  }

  closeDialog() {
    this.dialog.close();
  }

  isNotStdUser(){
    if(this.userSubType === 'std'){
      return false;
    }else{
      return true;
    }
  }
}
