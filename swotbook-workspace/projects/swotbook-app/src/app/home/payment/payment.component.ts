import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {LoginState} from '../+state/login.reducer';
import {Observable} from 'rxjs';
import {AppUserInfo, CurrentUser, OmiseCreditCard, SelectedPlan} from '../+state/login.model';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {OmiseAttachCreditCard, OmiseDeleteCard, OmiseGetCreditCard, OmiseScheduledCharge} from '../+state/login.actions';
import {AppUtilService} from '../../app.util.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy {

  currentUser$: Observable<CurrentUser>;
  currentUser: CurrentUser;
  creditCardForm: FormGroup;
  appUserInfo$: Observable<AppUserInfo>;
  appUserInfo: AppUserInfo;
  omiseId$: Observable<string>;
  omiseId: string;
  selectedPlan$: Observable<SelectedPlan>;
  selectedPlan: SelectedPlan;
  isLoading$: Observable<boolean>;
  displayPlan: String;
  displayPrice: String;
  displayPerPeriod: String;
  displayCurrency: String;
  displaySymbol: String;
  creditCardList$: Observable<any[]>;
  creditCardList: any[];
  selectedCreditCardId = '';

  isTermAccepted = false;
  isUSD = true;

  constructor(
    private loginStore: Store<LoginState>,
    private util: AppUtilService,
    private router: Router
  ) {

    this.currentUser$ = this.loginStore.select(state => state.login.currentUser);
    this.currentUser$.pipe(untilDestroyed(this)).subscribe((currentUser) => {
      this.currentUser = currentUser;
    });

    this.appUserInfo$ = this.loginStore.select(state => state.login.appUserInfo);
    this.appUserInfo$.pipe(untilDestroyed(this)).subscribe((appUserInfo) => {
      this.appUserInfo = appUserInfo;
    });

    this.omiseId$ = this.loginStore.select(state => state.login.appUserInfo.omiseId);
    this.omiseId$.pipe(untilDestroyed(this)).subscribe(omiseId => {

      if (omiseId) {
        this.loginStore.dispatch(new OmiseGetCreditCard(omiseId));
      }

    });

    this.creditCardList$ = this.loginStore.select(state => state.login.userCreditCardList);
    this.creditCardList$.pipe(untilDestroyed(this)).subscribe((cardList) => {
        this.creditCardList = cardList;
        if (this.creditCardList.length > 0) {
          this.selectedCreditCardId = this.creditCardList[0].id;
        } else {
          this.selectedCreditCardId = '';
        }
      }
    );


    this.selectedPlan$ = this.loginStore.select(state => state.login.selectedPlan);
    this.selectedPlan$.pipe(untilDestroyed(this)).subscribe((selectedPlan) => {

      this.selectedPlan = selectedPlan;
      if (this.selectedPlan.isSelected) {

        this.displayPerPeriod = this.selectedPlan.period === 'monthly' ? '/month' : '/year';
        this.displayPlan = this.selectedPlan.subType === 'prm' ? 'Premium' : 'Pro';

        this.isUSD = this.selectedPlan.currency === 'usd';
        this.displayCurrency = this.isUSD? 'USD':'THB';
        this.displaySymbol = this.isUSD? '$':'฿';


        if (this.selectedPlan.subType === 'prm') {
          if (this.selectedPlan.period === 'monthly') {

            this.displayPrice = this.isUSD? '$2.99':'฿99' ;

          } else if (this.selectedPlan.period === 'annually') {

            this.displayPrice = this.isUSD? '$29.9':'฿990';

          }

        } else if (this.selectedPlan.subType === 'pro') {

          if (this.selectedPlan.period === 'monthly') {

            this.displayPrice = this.isUSD? '$4.99':'฿159';

          } else if (this.selectedPlan.period === 'annually') {

            this.displayPrice = this.isUSD? '$49.9':'฿1,590';

          }
        }

      } else {
        this.router.navigate(['/pricing']);

      }

    });

    this.isLoading$ = this.loginStore.select(state => state.login.isLoading);

  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy(){

  }
  createForm() {
    this.creditCardForm = new FormGroup({
      creditCardNumber: new FormControl('', [Validators.required]),
      creditCardName: new FormControl('', [Validators.required]),
      expireMonth: new FormControl('', [Validators.required]),
      expireYear: new FormControl('', [Validators.required]),
      security: new FormControl('', [Validators.required]),
    });
  }

  getCreditCardNumber() {
    return this.creditCardForm.get('creditCardNumber').value;
  }

  addCard() {

    const card: OmiseCreditCard = {
      name: this.creditCardForm.get('creditCardName').value,
      number: this.creditCardForm.get('creditCardNumber').value.split(' ').join(''),
      expiration_month: this.creditCardForm.get('expireMonth').value,
      expiration_year: this.creditCardForm.get('expireYear').value,
      security_code: this.creditCardForm.get('security').value
    };

    // const card: OmiseCreditCard = {
    //   name: `KHANIN TEST`,
    //   number: '4242424242424242',
    //   expiration_month: '1',
    //   expiration_year: '30',
    //   security_code: 999
    // };

    this.loginStore.dispatch(new OmiseAttachCreditCard(this.appUserInfo.omiseId, card));

  }

  deleteCard(cardId: string) {
    this.loginStore.dispatch(new OmiseDeleteCard(this.appUserInfo.omiseId, cardId));
  }

  checkOut() {

    // alert('Site is undergoing payment system approval process. Thank you for your support and patience :)');

    this.loginStore.dispatch(new OmiseScheduledCharge(
      this.currentUser.uid,
      this.appUserInfo.omiseId,
      this.selectedCreditCardId,
      this.selectedPlan
    ));

  }

  getCreditCardIcon(cardBrand: string) {

    const iconPath = 'assets/icons/credit_card/';

    if (cardBrand === 'Visa') {
      return iconPath + 'visa_icon.png';
    } else if (cardBrand === 'Mastercard') {
      return iconPath + 'mastercard_icon.png';
    } else if (cardBrand === 'Americanexpress') {
      return iconPath + 'american_express_icon.png';
    } else if (cardBrand === 'Westernunion') {
      return iconPath + 'western_union_icon.png';
    } else {
      return iconPath + 'creditcard_icon.svg';
    }

  }

  isSelected(cardId) {

    return this.selectedCreditCardId === cardId;

  }
}
