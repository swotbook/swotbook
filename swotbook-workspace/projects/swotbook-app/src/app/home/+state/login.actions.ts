import {Action} from '@ngrx/store';
import {AppUserInfo, CurrentUser, OmiseCreditCard, OmiseRefund, OmiseSchedule, SelectedPlan, TimeStamp} from './login.model';

export enum LoginActionTypes {
  GET_USER = 'GET_USER',
  GET_USER_INFO = 'GET_USER_INFO',
  GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS',
  USER_AUTHENTICATED = 'USER_AUTHENTICATED',
  USER_NOT_AUTHENTICATED = 'USER_NOT_AUTHENTICATED',
  GOOGLE_LOGIN = 'GOOGLE_LOGIN',
  GOOGLE_LOGIN_SUCCESS = 'GOOGLE_LOGIN_SUCCESS',
  FACEBOOK_LOGIN = 'FACEBOOK_LOGIN',
  FACEBOOK_LOGIN_SUCCESS = 'FACEBOOK_LOGIN_SUCCESS',
  LOGOUT = 'LOGOUT',
  LOGOUT_SUCCESS = 'LOGOUT_SUCCESS',
  TEST_CLOUD_FUNCTIONS = 'TEST_CLOUD_FUNCTIONS',
  GET_CARD_TOKEN = 'GET_CARD_TOKEN',
  OMISE_CREATE_CUSTOMER = 'OMISE_CREATE_USER',
  OMISE_GET_CREDIT_CARD = 'OMISE_GET_CREDIT_CARD',
  OMISE_GET_CREDIT_CARD_SUCCESS = 'OMISE_GET_CREDIT_CARD_SUCCESS',
  OMISE_ATTACH_CREDIT_CARD = 'OMISE_ATTACH_CREDIT_CARD',
  OMISE_ATTACH_CREDIT_CARD_SUCCESS = 'OMISE_ATTACH_CREDIT_CARD_SUCCESS',
  OMISE_DELETE_CARD = 'OMISE_DELETE_CARD',
  OMISE_DELETE_CARD_SUCCESS = 'OMISE_DELETE_CARD_SUCCESS',
  OMISE_SCHEDULED_CHARGE = 'OMISE_SCHEDULED_CHARGE',
  OMISE_SCHEDULED_CHARGE_SUCCESS = 'OMISE_SCHEDULED_CHARGE_SUCCESS',
  SELECT_PLAN = 'SELECT_PLAN',
  OMISE_GET_SCHEDULED_CHARGE = 'OMISE_GET_SCHEDULED_CHARGE',
  NO_SUBSCRIPTION_ACTIVE = 'NO_SUBSCRIPTION_ACTIVE',
  CHECK_SUBSCRIPTION_AVAILABILITY = 'CHECK_SUBSCRIPTION_AVAILABILITY',
  SUBSCRIPTION_ACTIVE = 'SUBSCRIPTION_ACTIVE',
  SUBSCRIPTION_CANCELED = 'SUBSCRIPTION_CANCELED', // has day left in subscription
  SUBSCRIPTION_EXPIRED = 'SUBSCRIPTION_CANCELED', // has no day left in subscription
  OMISE_SCHEDULE_CHARGE_DELETE = 'OMISE_SCHEDULE_CHARGE_DELETE',
  OMISE_SCHEDULE_CHARGE_DELETE_SUCCESS = 'OMISE_SCHEDULE_CHARGE_DELETE_SUCCESS',
  OMISE_REFUND_CHARGE = 'OMISE_REFUND_CHARGE',
  OMISE_REFUND_CHARGE_SUCCESS = 'OMISE_REFUND_CHARGE_SUCCESS',
  SET_AUTH_REDIRECT = 'SET_AUTH_REDIRECT',
  CHECK_APP_VERSION = 'CHECK_APP_VERSION',
  CHECK_APP_VERSION_SUCCESS = 'CHECK_APP_VERSION_SUCCESS'

}

export class GetUser implements Action {
  readonly type = LoginActionTypes.GET_USER;

  constructor(public payload?: any) {
  }
}

export class GetUserInfo implements Action {
  readonly type = LoginActionTypes.GET_USER_INFO;

  constructor(public currentUser:CurrentUser) {
  }
}

export class GetUserInfoSuccess implements Action {
  readonly type = LoginActionTypes.GET_USER_INFO_SUCCESS;

  constructor(public currentUser:CurrentUser,public appUserInfo:AppUserInfo) {
  }
}

export class UserAuthenticated implements Action {
  readonly type = LoginActionTypes.USER_AUTHENTICATED;

  constructor(public uid: string, public userData: firebase.User) {
  }
}

export class UserNotAuthenticated implements Action {
  readonly type = LoginActionTypes.USER_NOT_AUTHENTICATED;

  constructor() {
  }
}

export class GoogleLogin implements Action {
  readonly type = LoginActionTypes.GOOGLE_LOGIN;

  constructor() {
  }
}

export class GoogleLoginSuccess implements Action {
  readonly type = LoginActionTypes.GOOGLE_LOGIN_SUCCESS;

  constructor() {
  }
}

export class FacebookLogin implements Action {
  readonly type = LoginActionTypes.FACEBOOK_LOGIN;

  constructor() {
  }
}

export class FacebookLoginSuccess implements Action {
  readonly type = LoginActionTypes.FACEBOOK_LOGIN_SUCCESS;

  constructor() {
  }
}

export class LogOut implements Action {
  readonly type = LoginActionTypes.LOGOUT;

  constructor() {
  }
}

export class LogOutSuccess implements Action {
  readonly type = LoginActionTypes.LOGOUT_SUCCESS;

  constructor() {
  }
}

export class TestCloudFunctions implements Action {
  readonly type = LoginActionTypes.TEST_CLOUD_FUNCTIONS;

  constructor() {
  }
}

export class GetCardToken implements Action {
  readonly type = LoginActionTypes.GET_CARD_TOKEN;

  constructor() {
  }
}

export class OmiseCreateCustomer implements Action {
  readonly type = LoginActionTypes.OMISE_CREATE_CUSTOMER;

  constructor(public currentUser: CurrentUser) {
  }
}

export class OmiseGetCreditCard implements Action {
  readonly type = LoginActionTypes.OMISE_GET_CREDIT_CARD;

  constructor(public omiseId: string) {
  }
}

export class OmiseGetCreditCardSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_GET_CREDIT_CARD_SUCCESS;

  constructor(public creditCardList: any[]) {
  }
}

export class OmiseAttachCreditCard implements Action {
  readonly type = LoginActionTypes.OMISE_ATTACH_CREDIT_CARD;

  constructor(public omiseId: string, public card:OmiseCreditCard) {
  }
}

export class OmiseAttachCreditCardSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_ATTACH_CREDIT_CARD_SUCCESS;

  constructor(public creditCardList: any[]) {
  }
}

export class OmiseDeleteCard implements Action {
  readonly type = LoginActionTypes.OMISE_DELETE_CARD;

  constructor(public omiseId: string, public cardId:string) {
  }
}

export class OmiseDeleteCardSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_DELETE_CARD_SUCCESS;

  constructor(public creditCardList:any[]) {
  }
}

export class OmiseScheduledCharge implements Action {
  readonly type = LoginActionTypes.OMISE_SCHEDULED_CHARGE;

  constructor(
    public currentUserId:string,
    public omiseId: string,
    public cardId:string,
    public selectedPlan:SelectedPlan,
) {
  }
}

export class OmiseScheduledChargeSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_SCHEDULED_CHARGE_SUCCESS;

  constructor(public schedule:any) {
  }
}

export class SelectPlan implements Action {
  readonly type = LoginActionTypes.SELECT_PLAN;
  constructor(public selectedPlan:SelectedPlan){}
}

export class OmiseGetScheduledCharge implements Action {
  readonly type = LoginActionTypes.OMISE_GET_SCHEDULED_CHARGE;
  constructor(public appUserInfo:AppUserInfo){}
}

export class CheckSubscriptionAvailability implements Action {
  readonly type = LoginActionTypes.CHECK_SUBSCRIPTION_AVAILABILITY;
  constructor(public schedule:OmiseSchedule, public appUserInfo:AppUserInfo){}
}

export class NoSubscriptionActive implements Action {
  readonly type = LoginActionTypes.NO_SUBSCRIPTION_ACTIVE;
  constructor(){}
}
export class SubscriptionActive implements Action {
  readonly type = LoginActionTypes.SUBSCRIPTION_ACTIVE;
  constructor(public subType:'std'|'prm'|'pro', public userDaysUntilSubEnd:number){}
}

export class OmiseScheduledChargeDelete implements Action {
  readonly type = LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE;
  constructor(public currentUserId:string,public schedule:OmiseSchedule){}
}

export class OmiseScheduledChargeDeleteSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE_SUCCESS;
  constructor(public schedule:OmiseSchedule){}
}

export class SetAuthRedirect implements Action {
  readonly type = LoginActionTypes.SET_AUTH_REDIRECT;
  constructor(public authRedirectPage:string){}
}

export class OmiseRefundCharge implements Action {
  readonly type = LoginActionTypes.OMISE_REFUND_CHARGE;
  constructor(public uid:string,
              public chargeId:string,
              public dayLeft:string,
              public selectedPlan:SelectedPlan,
              public chargedAmount:string){}
}

export class OmiseRefundChargeSuccess implements Action {
  readonly type = LoginActionTypes.OMISE_REFUND_CHARGE_SUCCESS;
  constructor(public refundObj:OmiseRefund, public uid:string){}
}

export class CheckAppVersion implements Action {
  readonly type = LoginActionTypes.CHECK_APP_VERSION;
  constructor(public currentVersion:string){}
}

export type LoginActions =
  | GetUser
  | UserAuthenticated
  | UserNotAuthenticated
  | GoogleLogin
  | GoogleLoginSuccess
  | LogOut
  | LogOutSuccess
  | TestCloudFunctions
  | FacebookLogin
  | FacebookLoginSuccess
  | GetCardToken
  | OmiseCreateCustomer
  | GetUserInfo
  | GetUserInfoSuccess
  | OmiseGetCreditCard
  | OmiseGetCreditCardSuccess
  | OmiseAttachCreditCard
  | OmiseAttachCreditCardSuccess
  | OmiseDeleteCard
  | OmiseDeleteCardSuccess
  | OmiseScheduledCharge
  | OmiseScheduledChargeSuccess
  | OmiseGetScheduledCharge
  | NoSubscriptionActive
  | CheckSubscriptionAvailability
  | SelectPlan
  | SubscriptionActive
  | OmiseScheduledChargeDelete
  | OmiseScheduledChargeDeleteSuccess
  | SetAuthRedirect
  | OmiseRefundCharge
  | OmiseRefundChargeSuccess
  | CheckAppVersion;

