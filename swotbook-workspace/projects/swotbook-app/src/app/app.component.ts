import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginState } from './home/+state/login.reducer';
import {CheckAppVersion, GetUser, LogOut} from './home/+state/login.actions';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import {CurrentVersion} from './app.version';

declare var gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // google analytic
  private currentRoute: string;

  constructor(
    public store: Store<LoginState>,
    public _router: Router,
    public _location: Location
  ) {

    this.store.dispatch(new GetUser());
    this.store.dispatch(new CheckAppVersion(CurrentVersion));

    window.onscroll = function () {
      window.scrollTo(0,window.scrollY);
    };

    _router.events.subscribe((event: Event) => {
      // Send GA tracking on NavigationEnd event. You may wish to add other
      // logic here too or change which event to work with
      if (event instanceof NavigationEnd) {

        // navigate to top when load navigation end.

        // When the route is '/', location.path actually returns ''.
        const newRoute = _location.path() || '/';
        // If the route has changed, send the new route to analytics.
        if (this.currentRoute !== newRoute) {
          // ga('send', 'pageview', newRoute);
          gtag('config', 'UA-114681421-1', {'page_path': `${newRoute}`});

          this.currentRoute = newRoute;

          window.scrollTo(0, 0);

        }
      }
    });

  }

}
