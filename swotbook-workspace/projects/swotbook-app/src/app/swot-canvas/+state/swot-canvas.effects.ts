import { Injectable, OnDestroy } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import {
  ProjectSub,
  ProjectSubSuccess,
  CanvasAdd, CanvasCopy,
  CanvasDelete,
  CanvasInProjectSub,
  CanvasInProjectSubSuccess,
  CanvasModify,
  CanvasSelect,
  CardAdd, CardBatchDelete,
  CardDelete,
  CardModify,
  CardMove,
  CardsInCanvasSub,
  SwotCanvasActionTypes,
  ProjectAdd,
  ProjectDelete,
  ProjectDeleteSuccess,
  ProjectModify,
  ProjectMove,
  ProjectSelect,
  ProjectUpdateActive
} from './swot-canvas.actions';
import { DataPersistence } from '@nrwl/nx';
import { swotCanvasInitialState, SwotCanvasState } from './swot-canvas.reducer';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { SwotCanvasModel, SwotCard, SwotProject } from './swot-canvas.model';
import { AppUtilService } from '../../app.util.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import * as swotActions from './swot-canvas.actions';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CurrentUser } from '../../home/+state/login.model';
import { LoginState } from '../../home/+state/login.reducer';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class SwotCanvasEffects implements OnDestroy {

  currentUser$: Observable<CurrentUser>;
  activeProject$: Observable<SwotProject>;
  activeCanvas$: Observable<SwotCanvasModel>;
  userSubType$: Observable<string>;
  userSubType:string;

  snackBarDuration = 1200;

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<SwotCanvasState>,
    private afs: AngularFirestore,
    private store: Store<SwotCanvasState>,
    private loginStore: Store<LoginState>,
    private util: AppUtilService,
    private snackBar: MatSnackBar
  ) {
    this.currentUser$ = this.loginStore.select(
      state => state.login.currentUser
    );
    this.userSubType$ = this.loginStore.select(
      state => state.login.userSubType
    );
    this.userSubType$.pipe(untilDestroyed(this)).subscribe(a => this.userSubType = a);

    this.activeProject$ = this.store.select(
      state => state.swotCanvas.activeProject
    );
    this.activeCanvas$ = this.store.select(
      state => state.swotCanvas.activeCanvas
    );
  }

  @Effect()
  ProjectSub$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_SUB,
    {
      id: (action: ProjectSub) => {
        return action.userId;
      },
      run: (action: ProjectSub) => {

        let projectSub$;

        projectSub$ = this.afs
          .collection(`swotUsers/${action.userId}/swotTimeline`, ref =>
            ref.orderBy('index', 'asc')
          )
          .snapshotChanges();

        projectSub$.pipe(untilDestroyed(this)).subscribe(allProject => {
          this.store.dispatch(
            new swotActions.ProjectSubSuccess(
              this.convertMetaToObj(allProject)
            )
          );
        });
      },
      onError: err => {}
    }
  );

  @Effect()
  ProjectSubSuccess$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_SUB_SUCCESS,
    {

      run: (action: ProjectSubSuccess) => {

        let activeProject: SwotProject;
        this.activeProject$.take(1).subscribe(a => (activeProject = a));

        if (action.allProject.length !== 0) {
          if (activeProject.id === '') {
            this.store.dispatch(new ProjectSelect(action.allProject[0]));
            this.store.dispatch(
              new CanvasInProjectSub(
                action.allProject[0].id,
                action.allProject[0].createdBy
              )
            );
          } else {
            const updatedActiveProject = action.allProject.find(
              project => project.id === activeProject.id
            );
            this.store.dispatch(
              new ProjectUpdateActive(updatedActiveProject)
            );
          }
        }
      },
      onError: err => {}
    }
  );

  @Effect()
  ProjectUpdateActive$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_UPDATE_ACTIVE,
    {
      run: (action: ProjectUpdateActive) => {

        this.store.dispatch(
          new CanvasInProjectSub(
            action.project.id,
            action.project.createdBy
          )
        );

      },
      onError: err => {}
    }
  );

  @Effect()
  ProjectAdd$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_ADD,
    {
      run: (action: ProjectAdd) => {
        this.afs
          .collection(`swotUsers/${action.timeline.createdBy}/swotTimeline`)
          .add(action.timeline)
          .then(res => {

            this.updateAppStat('createdProject',1);

            const newCanvas: SwotCanvasModel = {
              inTimeline: res.id,
              createdBy: action.timeline.createdBy,
              createdAt: this.util.getTimestampNow(),
              updatedAt: this.util.getTimestampNow(),
              title: 'Your first canvas',
              analysis: ''
            };

            return this.store.dispatch(new CanvasAdd(newCanvas));
          });
      },
      onError: err => {}
    }
  );

  @Effect()
  projectDelete$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_DELETE,
    {
      run: (action: ProjectDelete) => {
        const activeProject$ = this.store.select(
          state => state.swotCanvas.activeProject
        );
        activeProject$.take(1).subscribe(activeProject => {
          if (activeProject.id === action.toDeleteProject.id) {
            const emptyTimeline: SwotProject = {
              id: '',
              index: 0,
              title: '',
              description: '',
              createdBy: '',
              createdAt: this.util.getTimestampNow(),
              updatedAt: this.util.getTimestampNow(),
              row1Name: 'Internal',
              row2Name: 'External',
              col1Name: 'Positive',
              col2Name: 'Negative',
              row1col1Name: 'Strengths',
              row1col2Name: 'Weaknesses',
              row2col1Name: 'Opportunities',
              row2col2Name: 'Threats',
              isShowLabel: true,
              isTodo: false,
              row1col1Color: swotCanvasInitialState.activeProject.row1col1Color,
              row2col1Color: swotCanvasInitialState.activeProject.row2col1Color,
              row1col2Color: swotCanvasInitialState.activeProject.row1col2Color,
              row2col2Color: swotCanvasInitialState.activeProject.row2col2Color,
              defaultHideTimeline: false
            };

            this.store.dispatch(new ProjectSelect(emptyTimeline));
          }
        });

        const batch = this.afs.firestore.batch();
        const allProjectRef = this.afs.collection(
          `swotUsers/${action.toDeleteProject.createdBy}/swotTimeline`
        );

        const toDeleteProjectRef = this.afs.doc(`swotUsers/${action.toDeleteProject.createdBy}/swotTimeline/${action.toDeleteProject.id}`)
        toDeleteProjectRef.delete().then((deletedProject)=>{

          const allProject$ = this.store.select(
            state => state.swotCanvas.allTimeline
          );
          allProject$.pipe(take(1)).subscribe(allProject => {
            for (let i = action.toDeleteProject.index + 1; i < allProject.length; i++) {
              // console.log(`from ${i} to ${i - 1}: ${cardList[i].id}`);

              batch.update(allProjectRef.doc(allProject[i].id).ref, {
                index: i - 1
              });
            }
          });

          batch.commit().then(() => {

            this.updateAppStat('deletedProject',1);
            this.store.dispatch(new ProjectDeleteSuccess(action.toDeleteProject));
          });

        });

      },
      onError: err => {}
    }
  );

  @Effect()
  projectMove$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_MOVE,
    {
      run: (action: ProjectMove) => {

        const batch = this.afs.firestore.batch();
        const timelineRef = this.afs.collection(
          `swotUsers/${action.project.createdBy}/swotTimeline`
        );

        // update moving card
        batch.update(timelineRef.doc(action.project.id).ref, {
          index: action.toIndex
        });

        // update others card index
        const allTimeline$ = this.store.select(
          state => state.swotCanvas.allTimeline
        );
        allTimeline$.pipe(take(1)).subscribe(allTimeline => {
          if (action.toIndex < action.fromIndex) {
            // moving up
            for (let i = action.toIndex; i < action.fromIndex; i++) {
              // console.log(`from ${i} to ${i + 1}: ${fromCardList[i].id}`);

              batch.update(timelineRef.doc(allTimeline[i].id).ref, {
                index: i + 1
              });
            }
          } else if (action.toIndex > action.fromIndex) {
            // moving down

            for (let i = action.fromIndex + 1; i < action.toIndex + 1; i++) {
              // console.log(`from ${i} to ${i - 1}: ${fromCardList[i].id}`);

              batch.update(timelineRef.doc(allTimeline[i].id).ref, {
                index: i - 1
              });
            }
          }

          batch.commit();
        });
      },
      onError: err => {}
    }
  );

  @Effect()
  timelineModify$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.PROJECT_MODIFY,
    {
      run: (action: ProjectModify) => {
        const timelineRef = this.afs.collection(
          `swotUsers/${action.updatedTimeline.createdBy}/swotTimeline`
        );
        timelineRef.doc(action.updatedTimeline.id).update({
          title: action.updatedTimeline.title,
          description: action.updatedTimeline.description,
          row1Name: action.updatedTimeline.row1Name,
          row2Name: action.updatedTimeline.row2Name,
          col1Name: action.updatedTimeline.col1Name,
          col2Name: action.updatedTimeline.col2Name,
          row1col1Name: action.updatedTimeline.row1col1Name,
          row1col2Name: action.updatedTimeline.row1col2Name,
          row2col1Name: action.updatedTimeline.row2col1Name,
          row2col2Name: action.updatedTimeline.row2col2Name,
          isShowLabel: action.updatedTimeline.isShowLabel,
          isTodo: action.updatedTimeline.isTodo,
          row1col1Color: action.updatedTimeline.row1col1Color,
          row1col2Color: action.updatedTimeline.row1col2Color,
          row2col1Color: action.updatedTimeline.row2col1Color,
          row2col2Color: action.updatedTimeline.row2col2Color,
          defaultHideTimeline: action.updatedTimeline.defaultHideTimeline
        }).then(() => {
          // this.projectSettingDialogRef.close();
        });
      },
      onError: err => {
      }
    }
  );

  @Effect()
  CanvasInTimelineSub$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB,
    {
      id: (action: CanvasInProjectSub) => {
        return action.projectId;
      },
      run: (action: CanvasInProjectSub) => {

        let allCanvasSub$;

        allCanvasSub$ = this.afs
          .collection(
            `swotUsers/${action.userId}/swotTimeline/${
              action.projectId
            }/swotCanvas`,
            (ref) =>
              ref.orderBy('createdAt', 'desc')
          )
          .snapshotChanges();

        allCanvasSub$.pipe(untilDestroyed(this)).subscribe(allCanvas => {
          return this.store.dispatch(
            new swotActions.CanvasInProjectSubSuccess(
              this.convertMetaToObj(allCanvas)
            )
          );
        });
      },
      onError: err => {}
    }
  );

  @Effect()
  canvasInProjectSubSuccess$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB_SUCCESS,
    {
      run: (action: CanvasInProjectSubSuccess) => {

        let activeCanvas;
        this.activeCanvas$.take(1).subscribe(a => (activeCanvas = a));
        if (action.canvasInTimeline.length !== 0) {

          if (activeCanvas.id === '') {
            this.store.dispatch(new CanvasSelect(action.canvasInTimeline[0]));
            this.store.dispatch(new CardsInCanvasSub(action.canvasInTimeline[0]));
          } else {
            this.store.dispatch(
              new CanvasSelect(
                action.canvasInTimeline.find(
                  canvas => canvas.id === activeCanvas.id
                )
              )
            );
          }
        } else {

        }
      },
      onError: err => {}
    }
  );

  @Effect()
  CanvasAdd$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CANVAS_ADD, {
    run: (action: CanvasAdd) => {
      this.afs
        .collection(
          `swotUsers/${action.canvas.createdBy}/swotTimeline/${
            action.canvas.inTimeline
          }/swotCanvas`
        )
        .add(action.canvas)
        .then(res => {

          this.updateAppStat('createdCanvas',1);


        });
    },
    onError: err => {}
  });

  @Effect()
  CanvasCopy$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CANVAS_COPY, {
    run: (action: CanvasCopy) => {
      this.afs
        .collection(
          `swotUsers/${action.canvas.createdBy}/swotTimeline/${
            action.canvas.inTimeline
            }/swotCanvas`
        )
        .add(action.canvas)
        .then(res => {

          const batch = this.afs.firestore.batch();
          const cardsInSwotRef = this.afs.collection(
            `swotUsers/${action.canvas.createdBy}/swotTimeline/${
              action.canvas.inTimeline
              }/swotCanvas/${res.id}/swotCards`
          );

          for(let i = 0; i < action.copiedCardList.length; i++){

            action.copiedCardList[i].canvasId = res.id;
            batch.set(cardsInSwotRef.doc(action.copiedCardList[i].id).ref, action.copiedCardList[i]);

          }

          batch.commit().then((batchRes)=>{

            console.log(res);

            this.updateAppStat('createdCard',action.copiedCardList.length);

            this.snackBar.open('Canvas copied!', '', {
              duration: this.snackBarDuration
            });

            // this.store.dispatch(new CanvasSelect())

          });

        });
    },
    onError: err => {}
  });

  @Effect()
  CanvasDelete$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.CANVAS_DELETE,
    {
      run: (action: CanvasDelete) => {
        let activeCanvas;

        this.activeCanvas$.take(1).subscribe(a => (activeCanvas = a));

        if (action.canvas.id === activeCanvas.id) {
          const emptyIdCanvas: SwotCanvasModel = {
            ...action.canvas,
            id: ''
          };

          this.store.dispatch(new CanvasSelect(emptyIdCanvas));
        }

        this.afs
          .collection(
            `swotUsers/${action.canvas.createdBy}/swotTimeline/${
              action.canvas.inTimeline
            }/swotCanvas`
          )
          .doc(action.canvas.id)
          .delete()
          .then(res => {
            // console.log(res);
            this.updateAppStat('deletedCanvas',1);


          });
      },
      onError: err => {}
    }
  );

  @Effect()
  CardBatchDelete$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CARD_BATCH_DELETE, {
    run: (action: CardBatchDelete) => {

      const batch = this.afs.firestore.batch();
      const cardsInSwotRef = this.afs.collection(
        `swotUsers/${action.canvas.createdBy}/swotTimeline/${
          action.canvas.inTimeline
          }/swotCanvas/${action.canvas.id}/swotCards`
      );

      for(let i = 0; i < action.cardList.length; i++){


        batch.delete(cardsInSwotRef.doc(action.cardList[i].id).ref);

      }

      batch.commit().then(()=>{
        // this.snackBar.open('Canvas copied!', '', {
        //   duration: this.snackBarDuration
        // });
        this.updateAppStat('deletedCard',action.cardList.length);

      });

    },
    onError: err => {}
  });

  @Effect()
  canvasModify$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.CANVAS_MODIFY,
    {
      run: (action: CanvasModify) => {

        const canvasRef = this.afs.collection(
          `swotUsers/${action.canvas.createdBy}/swotTimeline/${
            action.canvas.inTimeline
          }/swotCanvas`
        );

        canvasRef.doc(action.canvas.id).update({
          title: action.canvas.title,
          analysis: action.canvas.analysis
        });
      },
      onError: err => {}
    }
  );

  @Effect()
  CardsInCanvasSub$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.CARDS_IN_CANVAS_SUB,
    {
      id: (action: CardsInCanvasSub) => {
        return action.canvas.id;
      },
      run: (action: CardsInCanvasSub) => {
        const allCardSub$ = this.afs
          .collection(
            `swotUsers/${action.canvas.createdBy}/swotTimeline/${
              action.canvas.inTimeline
            }/swotCanvas/${action.canvas.id}/swotCards`,
            ref => ref.orderBy('index', 'asc')
          )
          .snapshotChanges();

        allCardSub$.pipe(untilDestroyed(this)).subscribe(allCard => {
          return this.store.dispatch(
            new swotActions.CardsInCanvasSubSuccess(
              this.convertMetaToObj(allCard)
            )
          );
        });
      },
      onError: err => {}
    }
  );



  @Effect()
  CardAdd = this.dataPersistence.fetch(SwotCanvasActionTypes.CARD_ADD, {
    run: (action: CardAdd) => {

      this.afs
        .collection(
          `swotUsers/${action.card.createdBy}/swotTimeline/${
            action.canvas.inTimeline
          }/swotCanvas/${action.canvas.id}/swotCards`
        )
        .add(action.card)
        .then(res => {

          this.updateAppStat('createdCard',1);

          document.getElementById(`${res.id}title`).focus();

          this.snackBar.open('Card added!', '', {
              duration: this.snackBarDuration
          });

        });
    },
    onError: err => {}
  });

  @Effect()
  cardDelete$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CARD_DELETE, {
    run: (action: CardDelete) => {
      let activeCanvas: SwotCanvasModel;
      const activeCanvas$ = this.store.select(
        state => state.swotCanvas.activeCanvas
      );
      activeCanvas$.pipe(take(1)).subscribe(a => (activeCanvas = a));


      this.afs.collection(`swotUsers/${action.card.createdBy}/swotTimeline/${
        activeCanvas.inTimeline
        }/swotCanvas/${activeCanvas.id}/swotCards`).doc(action.card.id).delete().then(() => {

        this.updateAppStat('deletedCard',1);

        this.snackBar.open('Card deleted!', '', {
          duration: this.snackBarDuration
        });

        const batch = this.afs.firestore.batch();
        const cardsInSwotRef = this.afs.collection(
          `swotUsers/${action.card.createdBy}/swotTimeline/${
            activeCanvas.inTimeline
            }/swotCanvas/${activeCanvas.id}/swotCards`
        );

        const cardList$ = this.store.select(
          state => state.swotCanvas[`cardList${action.card.containIn}`]
        );
        cardList$.pipe(take(1)).subscribe(cardList => {
          for (let i = action.card.index ; i < cardList.length; i++) {
            // console.log(`from ${i} to ${i - 1}: ${cardList[i].id}`);

            batch.update(cardsInSwotRef.doc(cardList[i].id).ref, {
              lastModified: 'INDEX',
              index: i
            });
          }
        });

        batch.commit().then(()=>{

        });
      });

    },
    onError: err => {}
  });

  @Effect()
  cardModify$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CARD_MODIFY, {
    run: (action: CardModify) => {
      let activeCanvas: SwotCanvasModel;
      const activeCanvas$ = this.store.select(
        state => state.swotCanvas.activeCanvas
      );
      activeCanvas$.pipe(take(1)).subscribe(a => (activeCanvas = a));

      const cardsInSwotRef = this.afs.collection(
        `swotUsers/${action.card.createdBy}/swotTimeline/${
          activeCanvas.inTimeline
        }/swotCanvas/${activeCanvas.id}/swotCards`
      );
      cardsInSwotRef.doc(action.card.id).update({
        title: action.updatedCard.title,
        score: action.updatedCard.score,
        weight: action.updatedCard.weight,
        containIn: action.updatedCard.containIn,
        prevContainIn: action.updatedCard.prevContainIn,
        lastModified: action.updatedCard.lastModified,
        isChecked: action.updatedCard.isChecked
      });
    },
    onError: err => {}
  });

  @Effect()
  cardMove$ = this.dataPersistence.fetch(SwotCanvasActionTypes.CARD_MOVE, {
    run: (action: CardMove) => {
      let activeCanvas: SwotCanvasModel;
      const activeCanvas$ = this.store.select(
        state => state.swotCanvas.activeCanvas
      );
      activeCanvas$.pipe(take(1)).subscribe(a => (activeCanvas = a));

      const batch = this.afs.firestore.batch();
      const cardsInSwotRef = this.afs.collection(
        `swotUsers/${action.card.createdBy}/swotTimeline/${
          activeCanvas.inTimeline
        }/swotCanvas/${activeCanvas.id}/swotCards`
      );

      // Move Within Container
      if (action.fromType === action.toType) {
        // update moving card
        batch.update(cardsInSwotRef.doc(action.card.id).ref, {
          index: action.toIndex,
          lastModified: 'INDEX'
        });

        // update others card index
        const fromCardList$ = this.store.select(
          state => state.swotCanvas[`cardList${action.fromType}`]
        );
        fromCardList$.pipe(take(1)).subscribe(fromCardList => {
          // console.log(fromCardList);
          if (action.toIndex < action.fromIndex) {
            // moving up
            for (let i = action.toIndex; i < action.fromIndex; i++) {

              batch.update(cardsInSwotRef.doc(fromCardList[i].id).ref, {
                lastModified: 'INDEX',
                index: i + 1
              });
            }
          } else if (action.toIndex > action.fromIndex) {
            // moving down

            for (let i = action.fromIndex + 1; i < action.toIndex + 1; i++) {

              batch.update(cardsInSwotRef.doc(fromCardList[i].id).ref, {
                lastModified: 'INDEX',
                index: i - 1
              });
            }
          }

          batch.commit();
        });
      } else if (action.fromType !== action.toType) {
        // Move Between Container

        // update moving card
        batch.update(cardsInSwotRef.doc(action.card.id).ref, {
          containIn: action.toType,
          prevContainIn: action.fromType,
          index: action.toIndex,
          lastModified: 'MOVE'
        });

        // update old array of moving item with new index
        // console.log(`moving id ${action.card.id}`);
        const fromCardList$ = this.store.select(
          state => state.swotCanvas[`cardList${action.fromType}`]
        );
        fromCardList$.pipe(take(1)).subscribe(fromCardList => {
          for (let i = action.fromIndex + 1; i < fromCardList.length; i++) {

            batch.update(cardsInSwotRef.doc(fromCardList[i].id).ref, {
              lastModified: 'INDEX',
              index: i - 1
            });
          }
        });

        const toCardList$ = this.store.select(
          state => state.swotCanvas[`cardList${action.toType}`]
        );
        toCardList$.pipe(take(1)).subscribe(toCardList => {
          for (let i = action.toIndex; i < toCardList.length; i++) {

            batch.update(cardsInSwotRef.doc(toCardList[i].id).ref, {
              lastModified: 'INDEX',
              index: i + 1
            });
          }
        });

        batch.commit();
      } else {
      }
    },
    onError: err => {}
  });

  ngOnDestroy() {}

  convertMetaToObj(wrappedArray: {}[]) {
    const unwrappedArray = [];
    wrappedArray.forEach((meta: any) => {
      const item = {
        id: meta.payload.doc.id,
        ...meta.payload.doc.data()
      };
      unwrappedArray.push(item);
    });

    return unwrappedArray;
  }

  updateAppStat(field:string, changingValue:number){

    const docRef = this.afs.doc('stats/app').ref;

    this.afs.firestore.runTransaction(transaction => {
      return transaction.get(docRef).then((doc)=>{

        const newValue = doc.data()[field] + changingValue ;

        transaction.update(docRef,{[field]: newValue});

      });

    });
  }

}
