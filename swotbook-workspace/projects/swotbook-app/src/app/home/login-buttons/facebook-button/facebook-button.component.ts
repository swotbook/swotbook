import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginState } from '../../+state/login.reducer';
import { FacebookLogin } from '../../+state/login.actions';

@Component({
  selector: 'app-facebook-button',
  templateUrl: './facebook-button.component.html',
  styleUrls: ['./facebook-button.component.scss']
})
export class FacebookButtonComponent implements OnInit {

  constructor(
    public store: Store<LoginState>

  ) { }

  ngOnInit() {
  }


  facebookLogin() {

      // console.log('logging in using facebook');
    this.store.dispatch(new FacebookLogin());

  }
}
