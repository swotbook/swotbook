import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {
  GetUserInfo,
  GetUserInfoSuccess,
  LoginActionTypes,
  OmiseAttachCreditCard,
  OmiseAttachCreditCardSuccess,
  OmiseCreateCustomer,
  OmiseDeleteCard,
  OmiseGetCreditCard,
  OmiseGetCreditCardSuccess,
  OmiseGetScheduledCharge,
  CheckSubscriptionAvailability,
  OmiseScheduledCharge,
  OmiseScheduledChargeSuccess,
  UserAuthenticated,
  SubscriptionActive,
  OmiseScheduledChargeDelete,
  OmiseScheduledChargeDeleteSuccess,
  NoSubscriptionActive,
  OmiseRefundCharge, OmiseRefundChargeSuccess, GetUser, CheckAppVersion
} from './login.actions';
import * as loginActions from './login.actions';
import {Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {DataPersistence} from '@nrwl/nx';
import {LoginState} from './login.reducer';
import {HttpClient} from '@angular/common/http';

import * as firebase from 'firebase/app';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/switchMap';
import 'rxjs-compat/add/operator/catch';
import 'rxjs-compat/add/observable/fromPromise';
import 'rxjs-compat/add/observable/of';
import {SwotCanvasState} from '../../swot-canvas/+state/swot-canvas.reducer';
import {Store} from '@ngrx/store';
import {SwotInitActive} from '../../swot-canvas/+state/swot-canvas.actions';
import {AngularFirestore} from '@angular/fire/firestore';
import {AppUserInfo} from './login.model';
import {take} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {auth} from 'firebase/app';

declare var gtag: Function;
declare var Omise;

@Injectable()
export class LoginEffects {

  CLOUD_FN_URL = 'https://us-central1-swotbook-e5e1b.cloudfunctions.net';

  redirectPage$: Observable<string>;

  constructor(
    private actions$: Actions,
    public afAuth: AngularFireAuth,
    public router: Router,
    public dataPersistence: DataPersistence<LoginState>,
    private http: HttpClient,
    private swotStore: Store<SwotCanvasState>,
    private loginStore: Store<LoginState>,
    private afs: AngularFirestore,
    private dialog:MatDialog
  ) {

    this.redirectPage$ = this.loginStore.select(state => state.login.authRedirectPage);

  }

  @Effect()
    // GetUser$ = this.actions$
    //   .ofType(LoginActionTypes.GET_USER)
    //   .map(action => {
    //   })
    //   .switchMap(payload => this.afAuth.authState)
    //   .map(userData => {
    //     if (userData) {
    //       return new loginActions.UserAuthenticated(userData.uid, userData);
    //     } else {
    //       return new loginActions.UserNotAuthenticated();
    //     }
    //   })
    //   .catch(err => Observable.of());
  GetUser$ = this.dataPersistence.fetch(LoginActionTypes.GET_USER, {
    run: (action: GetUser) => {

      console.log('checking');

      this.afAuth.user.pipe().subscribe((user) => {
        if (user) {
          console.log('already have user');
          this.loginStore.dispatch(new loginActions.UserAuthenticated(user.uid, user));
        } else {
          console.log('no login user');
          this.loginStore.dispatch(new loginActions.UserNotAuthenticated());
        }
      });

    }
  });

  @Effect()
  UserAuthen$ = this.dataPersistence.fetch(
    LoginActionTypes.USER_AUTHENTICATED,
    {
      run: (action: UserAuthenticated) => {

        const dialogRef = this.dialog.closeAll();

        this.redirectPage$.pipe(take(1)).subscribe((redirectPage) => {
          this.router.navigate([redirectPage]);

        });

        this.loginStore.dispatch(new loginActions.GetUserInfo(action.userData));
      }
    }
  );


  @Effect()
  UserNotAuthen$ = this.dataPersistence.fetch(
    LoginActionTypes.USER_NOT_AUTHENTICATED,
    {
      run: () => {
        this.router.navigate(['']);

      }
    }
  );

  @Effect()
  GetUserInfo$ = this.dataPersistence.fetch(LoginActionTypes.GET_USER_INFO, {
    run: (action: GetUserInfo) => {

      console.log('getting user info');

      const userInfoDoc = this.afs.doc<AppUserInfo>(`appUserInfo/${action.currentUser.uid}`);
      const userInfoDoc$ = userInfoDoc.valueChanges();

      userInfoDoc$.pipe().first().subscribe(appUserInfo => {

        if (appUserInfo) {

          return this.loginStore.dispatch(new loginActions.GetUserInfoSuccess(action.currentUser, appUserInfo));

        } else {

          if (navigator.onLine) { // valueChanges also return undefined when browser is offline
            userInfoDoc.set({omiseId: '', activeChargeScheduleId: '', lastDateAvailable: '', lastOccurrence: ''});
          } else {

            alert('Please check your internet connection.');

          }

        }

      });

    }
  });

  @Effect()
  GetUserInfoSuccess$ = this.dataPersistence.fetch(LoginActionTypes.GET_USER_INFO_SUCCESS, {
    run: (action: GetUserInfoSuccess) => {

      if (action.appUserInfo.omiseId === '') {

        this.loginStore.dispatch(new loginActions.OmiseCreateCustomer(action.currentUser));

      } else {

        this.loginStore.dispatch((new loginActions.OmiseGetScheduledCharge(action.appUserInfo)));

      }

    }
  });

  @Effect()
  OmiseGetScheduledCharge$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_GET_SCHEDULED_CHARGE,
    {
      run: (action: OmiseGetScheduledCharge) => {

        if (action.appUserInfo.activeChargeScheduleId) {
          const options = {
            params: {
              scheduleId: action.appUserInfo.activeChargeScheduleId,

            }
          };

          this.http.get(`${this.CLOUD_FN_URL}/getScheduledCharge`, options).subscribe((schedule: any) => {

            this.loginStore.dispatch((new CheckSubscriptionAvailability(schedule, action.appUserInfo)));

          });
        } else {

          // console.log('no active schedule charge');
          this.loginStore.dispatch(new NoSubscriptionActive());

        }

      }
    }
  );

  @Effect()
  CheckSubscriptionAvailability$ = this.dataPersistence.fetch(
    LoginActionTypes.CHECK_SUBSCRIPTION_AVAILABILITY,
    {
      run: (action: CheckSubscriptionAvailability) => {

        if (action.schedule.status === 'active') {

          // update app user info and continue
          const description = action.schedule.charge.description;
          let subType: 'std' | 'prm' | 'pro' = 'std';

          if (description === 'prm' || description === 'pro') {
            subType = description;
          }

          this.loginStore.dispatch((new SubscriptionActive(subType, 0)));
          // this.router.navigate(['/swot']);

        } else {

          if (action.schedule.occurrences.data.length === 0) {// if no occurrences dispatch no subscription active

            this.loginStore.dispatch(new NoSubscriptionActive());

          } else {

            // check last current date vs last date available
            // if currentDate < lastDateAvailable : update user info and continue
            // else : do not update and continue
            const lastDate: any = new Date(action.appUserInfo.lastDateAvailable);
            const currentDate: any = new Date();

            const diff = Math.abs(currentDate - lastDate);
            const dayLeft = Math.floor(diff / (60 * 60 * 24 * 1000));

            if (currentDate < lastDate) {

              const description = action.schedule.charge.description;
              let subType: 'std' | 'prm' | 'pro' = 'std';

              if (description === 'prm' || description === 'pro') {
                subType = description;
              }

              this.loginStore.dispatch((new SubscriptionActive(subType, dayLeft)));

            } else {
              this.loginStore.dispatch(new NoSubscriptionActive());
            }

          }

        }

      }
    }
  );

  @Effect()
  OmiseCreateCustomer$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_CREATE_CUSTOMER,
    {
      run: (action: OmiseCreateCustomer) => {

        const config = {
          params: {
            uid: action.currentUser.uid,
            email: action.currentUser.email
          }
        };

        this.http.get(`${this.CLOUD_FN_URL}/createOmiseCustomer`, config).subscribe((res: any) => {
          // update appUserInfo with omise ID
          const userInfoDoc = this.afs.doc<AppUserInfo>(`appUserInfo/${action.currentUser.uid}`);
          userInfoDoc.update({
            omiseId: res.id,
          });

        });

      }
    }
  );

  @Effect()
  GoogleLogin$ = this.dataPersistence.fetch(LoginActionTypes.GOOGLE_LOGIN, {
    run: () => {

      gtag('config', 'UA-114681421-1', {'login_method': 'google'});

      // this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider())
      this.afAuth.signInWithPopup(new auth.GoogleAuthProvider())
        .catch(e => {
          if (e.code === 'auth/account-exists-with-different-credential') {
            alert('Email already create with Facebook, please home with Facebook account.');

          }
        });

    }
  });

  @Effect()
  LogOut$ = this.dataPersistence.fetch(LoginActionTypes.LOGOUT, {
    run: () => {
      this.swotStore.dispatch(new SwotInitActive());
      this.afAuth.signOut();
    }
  });


  @Effect()
  FacebookLogin$ = this.dataPersistence.fetch(LoginActionTypes.FACEBOOK_LOGIN, {
    run: () => {

      gtag('config', 'UA-114681421-1', {'login_method': 'facebook'});


      const fbProvider = new auth.FacebookAuthProvider();

      this.afAuth.signInWithPopup(fbProvider)
        .catch((e) => {
          if (e.code === 'auth/account-exists-with-different-credential') {
            alert('Email already create with Google, please home with Google account.');

          }

        });


    },
    onError: (error) => {

    }
  });


  @Effect()
  OmiseGetCreditCard$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_GET_CREDIT_CARD,
    {
      run: (action: OmiseGetCreditCard) => {

        const config = {
          params: {
            omiseId: action.omiseId,
          }
        };

        this.http.get(`${this.CLOUD_FN_URL}/getCreditCards`, config).subscribe((res: any) => {
          this.loginStore.dispatch(new OmiseGetCreditCardSuccess(res.data));

        });

      }
    }
  );

  @Effect()
  OmiseAttachCreditCard$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_ATTACH_CREDIT_CARD,
    {
      run: (action: OmiseAttachCreditCard) => {

        Omise.createToken('card', action.card, (statusCode, response) => {

          const config = {
            params: {
              omiseId: action.omiseId,
              cardToken: response.id
            }
          };

          this.http.get(`${this.CLOUD_FN_URL}/attachCreditCard`, config).subscribe((res: any) => {
            this.loginStore.dispatch(new OmiseAttachCreditCardSuccess(res.cards.data));

          });

        });


      },onError:(error)=> {
        alert(error);

      }
    }
  );

  @Effect()
  OmiseDeleteCreditCard = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_DELETE_CARD,
    {
      run: (action: OmiseDeleteCard) => {

        const config = {
          params: {
            omiseId: action.omiseId,
            cardId: action.cardId
          }
        };

        this.http.get(`${this.CLOUD_FN_URL}/deleteCreditCard`, config).subscribe((res: any) => {
          // this.loginStore.dispatch(new OmiseDeleteCardSuccess(res.cards.data));

          this.loginStore.dispatch(new OmiseGetCreditCard(action.omiseId));

        });

      }
    }
  );

  @Effect()
  OmiseScheduledCharge$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_SCHEDULED_CHARGE,
    {
      run: (action: OmiseScheduledCharge) => {

        const options = {
          params: {
            omiseId: action.omiseId,
            cardId: action.cardId,
            userSubType: action.selectedPlan.subType,
            period: action.selectedPlan.period,
            currency: action.selectedPlan.currency

          }
        };


        this.http.get(`${this.CLOUD_FN_URL}/scheduledCharge`, options).subscribe((schedule: any) => {

          const userInfoDoc = this.afs.doc<AppUserInfo>(`appUserInfo/${action.currentUserId}`);
          userInfoDoc.update({
            activeChargeScheduleId: schedule.id,
          });

          this.loginStore.dispatch((new OmiseGetScheduledCharge(schedule.id)));
          this.loginStore.dispatch((new OmiseScheduledChargeSuccess(schedule)));
        });

      }
    }
  );

  @Effect()
  OmiseScheduledChargeSuccess$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_SCHEDULED_CHARGE_SUCCESS,
    {
      run: (action: OmiseScheduledChargeSuccess) => {

        this.router.navigate(['/success']);

      }
    }
  );

  @Effect()
  OmiseScheduleChargeDelete$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE,
    {
      run: (action: OmiseScheduledChargeDelete) => {


        let nextOccurrence = '';
        if (action.schedule.next_occurrence_dates.length > 0) {
          nextOccurrence = action.schedule.next_occurrence_dates[0];
        }
        let lastOccurrence = '';
        const occurrencesData = action.schedule.occurrences.data;
        const dataLength = occurrencesData.length;
        if (dataLength > 0) {
          lastOccurrence = occurrencesData[dataLength - 1].schedule_date.toString();
        }

        const userInfoDoc = this.afs.doc<AppUserInfo>(`appUserInfo/${action.currentUserId}`);
        userInfoDoc.update({
          lastDateAvailable: nextOccurrence,
          lastOccurrence: lastOccurrence
        }).then((userInfo) => {

          const options = {
            params: {
              scheduleId: action.schedule.id,
            }
          };

          this.http.get(`${this.CLOUD_FN_URL}/deleteScheduledCharge`, options).subscribe((schedule: any) => {

            this.loginStore.dispatch((new OmiseScheduledChargeDeleteSuccess(schedule)));
          });

        });

      }
    }
  );

  @Effect()
  OmiseScheduleChargeSuccess$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_SCHEDULE_CHARGE_DELETE_SUCCESS,
    {
      run: (action: OmiseScheduledChargeDeleteSuccess) => {

        const appUserInfo$ = this.loginStore.select(state => state.login.appUserInfo);

        appUserInfo$.pipe(take(1)).subscribe(appUserInfo => {
          this.loginStore.dispatch(new CheckSubscriptionAvailability(action.schedule, appUserInfo));

        });

      }
    }
  );


  @Effect()
  OmiseRefundCharge$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_REFUND_CHARGE,
    {
      run: (action: OmiseRefundCharge) => {

        const options = {
          params: {
            chargeId: action.chargeId,
            dayLeft: action.dayLeft,
            period: action.selectedPlan.period,
            subType: action.selectedPlan.subType,
            currency: action.selectedPlan.currency,
            amount: action.chargedAmount

          }
        };

        this.http.get(`${this.CLOUD_FN_URL}/refundCharge`, options).subscribe((refund: any) => {

          this.loginStore.dispatch((new OmiseRefundChargeSuccess(refund, action.uid)));
        });
      }
    }
  );


  @Effect()
  OmiseRefundChargeSuccess$ = this.dataPersistence.fetch(
    LoginActionTypes.OMISE_REFUND_CHARGE_SUCCESS,
    {
      run: (action: OmiseRefundCharge) => {

        const userInfoDoc = this.afs.doc<AppUserInfo>(`appUserInfo/${action.uid}`);
        userInfoDoc.update({
          activeChargeScheduleId: ''
        }).then(() => {

        });


      }
    }
  );

  @Effect()
  CheckAppVersion = this.dataPersistence.fetch(
    LoginActionTypes.CHECK_APP_VERSION,
    {
      run: (action: CheckAppVersion) => {

        // console.log('checking version...');

        this.http.get(`${this.CLOUD_FN_URL}/getCurrentVersion`).subscribe((res: any) => {

          // console.log(`latest version ${res}`);
          // console.log(`current version ${action.currentVersion}`)

          if(res === action.currentVersion){
            // match current version do nothing
            // console.log(`up to date.`);
          }else{
            // not match version, force reload

            alert('Your app is out of date. Please hard refresh your browser to update.');
            // console.log('please refresh to update...');
            // location.reload();

          }

        });

      }
    }
  );

}
