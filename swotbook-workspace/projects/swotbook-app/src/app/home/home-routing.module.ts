import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { PricingComponent } from './pricing/pricing.component';
import { WipPageComponent } from './wip-page/wip-page.component';
import {PaymentComponent} from './payment/payment.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {SubscriptionSuccessComponent} from './subscription-success/subscription-success.component';

const routes: Routes = [
  { path: '', component: HomePageComponent, pathMatch: 'full' },
  { path: 'pricing', component: PricingComponent, pathMatch: 'full' },
  { path: 'payment', component: PaymentComponent, pathMatch: 'full' },
  { path: 'wip', component: WipPageComponent, pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent, pathMatch: 'full' },
  { path: 'success', component: SubscriptionSuccessComponent, pathMatch: 'full' },

  {
    path: 'terms',
    loadChildren: '../terms/terms.module#TermsModule'
  }

  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
