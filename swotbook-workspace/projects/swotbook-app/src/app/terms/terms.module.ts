import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsRoutingModule } from './terms-routing.module';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import {HomeModule} from '../home/home.module';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { DataProtectionComponent } from './data-protection/data-protection.component';

@NgModule({
  declarations: [PrivacyPolicyComponent, TermsAndConditionsComponent, DataProtectionComponent],
  imports: [
    CommonModule,
    TermsRoutingModule,
    HomeModule
  ]
})
export class TermsModule { }
