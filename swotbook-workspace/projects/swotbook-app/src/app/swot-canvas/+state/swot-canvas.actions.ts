import { Action } from '@ngrx/store';
import { SwotCanvasModel, SwotCard, SwotProject } from './swot-canvas.model';

export enum SwotCanvasActionTypes {
  PROJECT_SUB = 'ALL_PROJECT_SUB',
  PROJECT_SUB_SUCCESS = 'ALL_PROJECT_SUB_SUCCESS',
  PROJECT_ADD = 'PROJECT_ADD',
  PROJECT_SELECT = 'PROJECT_SELECT',
  PROJECT_DELETE = 'PROJECT_DELETE',
  PROJECT_DELETE_SUCCESS = 'PROJECT_DELETE_SUCCESS',
  PROJECT_MOVE = 'PROJECT_MOVE',
  PROJECT_MODIFY = 'PROJECT_MODIFY',
  PROJECT_UPDATE_ACTIVE = 'PROJECT_UPDATE_ACTIVE',
  CANVAS_IN_PROJECT_SUB = 'CANVAS_IN_TIMELINE_SUB',
  CANVAS_IN_PROJECT_SUB_SUCCESS = 'CANVAS_IN_TIMELINE_SUB_SUCCESS',
  CANVAS_ADD = 'CANVAS_ADD',
  CANVAS_DELETE = 'CANVAS_DELETE',
  CANVAS_SELECT = 'CANVAS_SELECT',
  CANVAS_MODIFY = 'CANVAS_MODIFY',
  CANVAS_COPY = 'CANVAS_COPY',
  CARDS_IN_CANVAS_SUB = 'CARDS_IN_CANVAS_SUB',
  CARDS_IN_CANVAS_SUB_SUCCESS = 'CARDS_IN_CANVAS_SUB_SUCCESS',
  CARD_ADD = 'CARD_ADD',
  CARD_DELETE = 'CARD_DELETE',
  CARD_BATCH_DELETE = 'CARD_BATCH_DELETE',
  CARD_MODIFY = 'CARD_MODIFY',
  CARD_MOVE = 'CARD_MOVE',
  SWOT_INIT_ACTIVE = 'SWOT_INIT_ACTIVE',
}

export class ProjectSub implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_SUB;

  constructor(public userId: string) {}
}

export class ProjectSubSuccess implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_SUB_SUCCESS;

  constructor(public allProject: SwotProject[]) {}
}

export class ProjectAdd implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_ADD;

  constructor(public timeline: SwotProject) {}
}

export class ProjectSelect implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_SELECT;

  constructor(public project: SwotProject) {}
}

export class ProjectUpdateActive implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_UPDATE_ACTIVE;

  constructor(public project: SwotProject) {}
}

export class ProjectModify implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_MODIFY;

  constructor(public updatedTimeline: SwotProject) {}
}

export class ProjectMove implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_MOVE;

  constructor(
    public project: SwotProject,
    public fromIndex: number,
    public toIndex: number
  ) {}
}

export class ProjectDelete implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_DELETE;

  constructor(public toDeleteProject: SwotProject) {}
}

export class ProjectDeleteSuccess implements Action {
  readonly type = SwotCanvasActionTypes.PROJECT_DELETE_SUCCESS;

  constructor(public deletedProject: SwotProject) {}
}

export class CanvasInProjectSub implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB;

  constructor(public projectId: string, public userId: string) {}
}

export class CanvasInProjectSubSuccess implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB_SUCCESS;

  constructor(public canvasInTimeline: SwotCanvasModel[]) {}
}

export class CanvasAdd implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_ADD;

  constructor(public canvas: SwotCanvasModel) {}
}

export class CanvasDelete implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_DELETE;

  constructor(public canvas: SwotCanvasModel) {}
}

export class CanvasSelect implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_SELECT;

  constructor(public canvas: SwotCanvasModel) {}
}

export class CanvasModify implements Action {
  readonly type = SwotCanvasActionTypes.CANVAS_MODIFY;

  constructor(public canvas: SwotCanvasModel) {}
}

export class CanvasCopy implements Action{
  readonly  type = SwotCanvasActionTypes.CANVAS_COPY;
  constructor(public  canvas: SwotCanvasModel, public copiedCardList:SwotCard[]){}
}

export class CardsInCanvasSub implements Action {
  readonly type = SwotCanvasActionTypes.CARDS_IN_CANVAS_SUB;

  constructor(public canvas: SwotCanvasModel) {}
}

export class CardsInCanvasSubSuccess implements Action {
  readonly type = SwotCanvasActionTypes.CARDS_IN_CANVAS_SUB_SUCCESS;

  constructor(public allCard: SwotCard[]) {}
}

export class CardAdd implements Action {
  readonly type = SwotCanvasActionTypes.CARD_ADD;

  constructor(public card: SwotCard, public canvas: SwotCanvasModel) {}
}

export class CardDelete implements Action {
  readonly type = SwotCanvasActionTypes.CARD_DELETE;

  constructor(public card: SwotCard) {}
}

export class CardBatchDelete implements Action {
  readonly type = SwotCanvasActionTypes.CARD_BATCH_DELETE;

  constructor(public canvas:SwotCanvasModel,public cardList: SwotCard[]) {}
}

export class CardModify implements Action {
  readonly type = SwotCanvasActionTypes.CARD_MODIFY;

  constructor(public card: SwotCard, public updatedCard: SwotCard) {}
}

export class CardMove implements Action {
  readonly type = SwotCanvasActionTypes.CARD_MOVE;

  constructor(
    public card: SwotCard,
    public fromType: 'S' | 'W' | 'O' | 'T' | 'N',
    public toType: 'S' | 'W' | 'O' | 'T' | 'N',
    public fromIndex: any,
    public toIndex: any
  ) {}
}

export class SwotInitActive implements Action {
  readonly  type = SwotCanvasActionTypes.SWOT_INIT_ACTIVE;
}

export type SwotCanvasActions =
  | ProjectSub
  | ProjectSubSuccess
  | ProjectAdd
  | ProjectDelete
  | ProjectDeleteSuccess
  | ProjectModify
  | ProjectSelect
  | ProjectUpdateActive
  | CanvasInProjectSub
  | CanvasInProjectSubSuccess
  | CanvasAdd
  | CanvasDelete
  | CanvasSelect
  | CanvasModify
  | CardsInCanvasSub
  | CardsInCanvasSubSuccess
  | CardAdd
  | CardDelete
  | CardBatchDelete
  | CardModify
  | CanvasCopy
  | SwotInitActive
  ;

