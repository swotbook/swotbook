import { Component, OnInit } from '@angular/core';
import '@firebase/auth';
import { Observable } from 'rxjs';
import { CurrentUser } from '../+state/login.model';
import { Store } from '@ngrx/store';
import { LoginState } from '../+state/login.reducer';
import 'rxjs-compat/add/operator/first';
import {LoginSignupDialogComponent} from '../dialogs/login-signup-dialog/login-signup-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-login-main',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  currentUser$: Observable<CurrentUser>;

  constructor(
    public store: Store<LoginState>,
    private dialog: MatDialog
  ) {
    this.currentUser$ = this.store.select(state => state.login.currentUser);
  }

  ngOnInit() {}

  openDialog(title:'Login'|'Sign up') {

    // let config = new MdDialogConfig();
    // let dialogRef:MdDialogRef<PizzaDialog> = this.dialog.open(PizzaDialog, config);
    // dialogRef.componentInstance.name = "Ham and Pineapple";
    // dialogRef.componentInstance.size = "Large";

    const dialogRef = this.dialog.open(LoginSignupDialogComponent, {
      height: 'auto',
      width: '95%',
      maxWidth: '400px'
    });

    dialogRef.componentInstance.title = title;

    dialogRef.afterClosed().subscribe(() => {

    });

  }

}
