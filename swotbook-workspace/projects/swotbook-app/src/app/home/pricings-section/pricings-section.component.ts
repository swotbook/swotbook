import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import {CurrentUser, AppUserInfo, SelectedPlan, OmiseSchedule} from '../+state/login.model';
import { Store } from '@ngrx/store';
import { LoginState } from '../+state/login.reducer';
import { Observable } from 'rxjs';
import {OmiseRefundCharge, OmiseScheduledChargeDelete, SelectPlan, SetAuthRedirect} from '../+state/login.actions';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {error} from 'util';

declare var gtag: Function;

@Component({
  selector: 'app-pricings-section',
  templateUrl: './pricings-section.component.html',
  styleUrls: ['./pricings-section.component.scss']
})
export class PricingsSectionComponent implements OnInit, OnDestroy {

  @Input() public backgroundGray;

  isAnnualSelected = false;

  appUserInfo$: Observable<AppUserInfo>;
  appUserInfo: AppUserInfo;
  currentUser$: Observable <CurrentUser>;
  currentUser: CurrentUser;
  userChargeSchedule$: Observable<OmiseSchedule>;
  userChargeSchedule: OmiseSchedule;
  userSubType$ : Observable<'std'|'prm'|'pro'>;
  userSubType: 'std'|'prm'|'pro';
  userDayLeft$:Observable<number>;
  userDayLeft:number;
  isUserSub = false;
  hasOccurrence = false;
  isLoading$:Observable<boolean>;
  currency: 'thb'|'usd' = 'usd';
  isUSD = true;

  constructor(
    private loginStore: Store<LoginState>,
    public router: Router

  ) {

    this.appUserInfo$ = this.loginStore.select(state => state.login.appUserInfo);
    this.appUserInfo$.pipe(untilDestroyed(this)).subscribe(a => this.appUserInfo = a);

    this.currentUser$ = this.loginStore.select(state => state.login.currentUser);
    this.currentUser$.pipe(untilDestroyed(this)).subscribe(a => this.currentUser = a);

    this.userChargeSchedule$ = this.loginStore.select(state => state.login.userChargeSchedule);
    this.userChargeSchedule$.pipe(untilDestroyed(this)).subscribe((a) => {
      this.userChargeSchedule = a;
      if(this.userChargeSchedule.every === 12){
        this.isAnnualSelected = true;
      }

      this.hasOccurrence = this.userChargeSchedule.occurrences.data.length !== 0;

    }

    );

    this.userSubType$ = this.loginStore.select(state => state.login.userSubType);
    this.userSubType$.pipe(untilDestroyed(this)).subscribe(a => {
      this.userSubType = a;
      this.isUserSub = this.userSubType !== 'std';
    });

    this.userDayLeft$ = this.loginStore.select(state => state.login.userDaysUntilSubEnd);
    this.userDayLeft$.pipe(untilDestroyed(this)).subscribe(a => this.userDayLeft = a);

    this.isLoading$ = this.loginStore.select(state =>state.login.isLoading);

  }

  ngOnInit() {
  }

  selectPlan(subType: 'std'|'prm'|'pro') {

    gtag('config', 'UA-114681421-1', {'price_button': `${subType}`});

    if(subType === 'pro'){ // Temp work in progress

      this.router.navigate(['/wip']);

    }else{

      const selectedPlan:SelectedPlan = {
        isSelected:true,
        subType:subType,
        period: this.isAnnualSelected? 'annually': 'monthly',
        currency: this.isUSD? 'usd':'thb'
      };

      this.loginStore.dispatch(new SelectPlan(selectedPlan));

      if(this.currentUser.uid){
        this.router.navigate(['/payment']);

      }else{
        this.loginStore.dispatch(new SetAuthRedirect('/payment'));
        this.router.navigate(['/login']);
      }
    }




  }

  cancelSubscription(){

    this.loginStore.dispatch(new OmiseScheduledChargeDelete(this.currentUser.uid,this.userChargeSchedule));

  }

  refundCharge(){

    const occurrencesData = this.userChargeSchedule.occurrences.data; // data is chronological, meaning it is from earliest to latest
    const chargeId = occurrencesData[occurrencesData.length - 1].result;
    let period: 'monthly'|'annually';
    let currency: 'thb'|'usd';
    const chargedAmount = this.userChargeSchedule.charge.amount;

    if(this.userChargeSchedule.every === 1){
      period = 'monthly';
    }else{
      period = 'annually';
    }

    if(this.userChargeSchedule.charge.currency === 'thb'){
      currency = 'thb';
    }else if(this.userChargeSchedule.charge.currency === 'usd'){
      currency = 'usd';
    }else{
      throw error('undefined currency');
    }

    const selectedPlan:SelectedPlan={
      period:period,
      subType: this.userSubType,
      currency: currency
    };

    this.loginStore.dispatch(
      new OmiseRefundCharge(
        this.currentUser.uid,
        chargeId,
        this.userDayLeft.toString(),
        selectedPlan,
        chargedAmount.toString()));

  }

  displayPeriod(){

    return this.isAnnualSelected? 'Annual payment': 'Monthly payment';

  }

  ngOnDestroy(){

  }

}
