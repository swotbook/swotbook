import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {CurrentUser} from '../+state/login.model';
import {Store} from '@ngrx/store';
import {LoginState} from '../+state/login.reducer';
import {GoogleLogin, LogOut, GetCardToken} from '../+state/login.actions';
import {MatDialog} from '@angular/material/dialog';
import {LoginSignupDialogComponent} from '../dialogs/login-signup-dialog/login-signup-dialog.component';

@Component({
  selector: 'app-header-section',
  templateUrl: './header-section.component.html',
  styleUrls: ['./header-section.component.scss']
})
export class HeaderSectionComponent implements OnInit {

  currentUser$: Observable<CurrentUser>;
  isLoading$: Observable<boolean>;

  constructor(
    public store: Store<LoginState>,
    private dialog: MatDialog
  ) {
    this.isLoading$ = this.store.select(state => state.login.isLoading);
    this.currentUser$ = this.store.select(state => state.login.currentUser);
  }

  ngOnInit() {
  }

  logOut() {
    this.store.dispatch(new LogOut());
  }

  loggingUid() {
    this.currentUser$.first().subscribe(user => {
    });
  }

  openDialog(title:'Login'|'Sign up') {

    // let config = new MdDialogConfig();
    // let dialogRef:MdDialogRef<PizzaDialog> = this.dialog.open(PizzaDialog, config);
    // dialogRef.componentInstance.name = "Ham and Pineapple";
    // dialogRef.componentInstance.size = "Large";

    const dialogRef = this.dialog.open(LoginSignupDialogComponent, {
      height: 'auto',
      width: '95%',
      maxWidth: '400px'
    });

    dialogRef.componentInstance.title = title;

    dialogRef.afterClosed().subscribe(() => {

    });

  }

}
