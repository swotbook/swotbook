import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-testimonials-section',
  templateUrl: './testimonials-section.component.html',
  styleUrls: ['./testimonials-section.component.scss']
})
export class TestimonialsSectionComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  public testimonials = [{
    logo: 'assets/images/mock-logo-4.png',
    photo: 'assets/images/face-1.jpg',
    text: `“I’ve tried using different softwares. The computer is not my strong side.
    There is excellent support behind DevEgret and people to walk you through it.
    If you have any questions they’ll go over that and explain to you how to do that. ”`,
    title: 'Jhone Doe',
    subtitle: 'Product Manager'
  }, {
    logo: 'assets/images/mock-logo-2.png',
    photo: 'assets/images/face-2.jpg',
    text: `"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius,
    atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."`,
    title: 'Adam Smith',
    subtitle: 'CEO'
  }, {
    logo: 'assets/images/mock-logo-3.png',
    photo: 'assets/images/face-3.jpg',
    text: `"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius,
    atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."`,
    title: 'Jhone White',
    subtitle: 'Software Engineer'
  }];

  ngOnInit() {

  }

}
