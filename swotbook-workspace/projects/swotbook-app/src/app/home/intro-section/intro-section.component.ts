import { Component, OnInit } from '@angular/core';
import { GoogleLogin } from '../+state/login.actions';
import { Store } from '@ngrx/store';
import { LoginState } from '../+state/login.reducer';
import { Observable } from 'rxjs';
import { CurrentUser } from '../+state/login.model';

@Component({
  selector: 'app-intro-two',
  templateUrl: './intro-section.component.html',
  styleUrls: ['./intro-section.component.scss']
})
export class IntroSectionComponent implements OnInit {

  currentUser$: Observable<CurrentUser>;

  constructor(
    public store: Store<LoginState>

  ) {

    this.currentUser$ = this.store.select(state => state.login.currentUser);


  }

  ngOnInit() {
  }


}
