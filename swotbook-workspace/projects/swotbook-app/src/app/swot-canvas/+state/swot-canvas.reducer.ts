import { SwotCanvasActions, SwotCanvasActionTypes } from './swot-canvas.actions';
import { SwotCanvasModel, SwotCard, SwotProject } from './swot-canvas.model';
import * as firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export interface SwotCanvas {
  loading: boolean;
  allTimeline: SwotProject[];
  activeProject: SwotProject;
  allCanvas: SwotCanvasModel[];
  activeCanvas: SwotCanvasModel;
  cardListAll: SwotCard[];
  cardListN: SwotCard[];
  cardListS: SwotCard[];
  cardListW: SwotCard[];
  cardListO: SwotCard[];
  cardListT: SwotCard[];
  trigger: boolean;
}

export const swotCanvasInitialState: SwotCanvas = {
  loading: false,
  cardListN: [],
  cardListS: [],
  cardListW: [],
  cardListO: [],
  cardListT: [],
  cardListAll: [],
  allTimeline: [],
  activeProject: {
    id: '',
    index: 0,
    title: '',
    description: '',
    createdBy: '',
    createdAt: '',
    updatedAt: '',
    row1Name: '',
    row2Name: '',
    col1Name: '',
    col2Name: '',
    row1col1Name: '',
    row1col2Name: '',
    row2col1Name: '',
    row2col2Name: '',
    isShowLabel: true,
    isTodo: true,
    row1col1Color: '#f5f5f5',
    row1col2Color: '#f5f5f5',
    row2col1Color: '#f5f5f5',
    row2col2Color: '#f5f5f5',
    defaultHideTimeline: false
  },
  allCanvas: [],
  trigger: false,
  activeCanvas: {
    id: '',
    inTimeline: '',
    createdAt: '',
    updatedAt: '',
    createdBy: '',
    title: '',
    allCard: [],
    analysis: ''
  }
};

export interface SwotCanvasState {
  readonly swotCanvas: SwotCanvas;
}

export function swotCanvasReducer(
  state = swotCanvasInitialState,
  action: SwotCanvasActions
): SwotCanvas {
  switch (action.type) {
    case SwotCanvasActionTypes.PROJECT_SUB: {
      return { ...state, loading: true };
    }

    case SwotCanvasActionTypes.PROJECT_SUB_SUCCESS: {
      return { ...state, loading: false, allTimeline: action.allProject };
    }

    case SwotCanvasActionTypes.PROJECT_SELECT: {
      return {
        ...state,
        activeProject: action.project,
        allCanvas: [],
        cardListN: [],
        cardListS: [],
        cardListW: [],
        cardListO: [],
        cardListT: [],
        activeCanvas: {
          id: '',
          inTimeline: '',
          createdBy: '',
          createdAt: new Timestamp(0, 0),
          updatedAt: new Timestamp(0, 0),
          title: '',
          analysis: ''
        }
      };
    }

    case SwotCanvasActionTypes.PROJECT_UPDATE_ACTIVE: {
      return {
        ...state,
        activeProject: action.project
      };
    }

    case SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB: {
      return { ...state, loading: true };
    }

    case SwotCanvasActionTypes.CANVAS_IN_PROJECT_SUB_SUCCESS: {
      return { ...state, loading: false, allCanvas: action.canvasInTimeline };
    }

    case SwotCanvasActionTypes.CANVAS_SELECT: {
      return {
        ...state,
        activeCanvas: action.canvas
      };
    }

    case SwotCanvasActionTypes.CARDS_IN_CANVAS_SUB: {
      return {
        ...state,
        loading: true,
        cardListN: [],
        cardListS: [],
        cardListW: [],
        cardListO: [],
        cardListT: []
      };
    }

    case SwotCanvasActionTypes.SWOT_INIT_ACTIVE: {
      return {
        ...state,
        activeProject: {...state.activeProject, id: ''},
        activeCanvas: {...state.activeCanvas, id: ''}
      };
    }

    case SwotCanvasActionTypes.CARDS_IN_CANVAS_SUB_SUCCESS: {
      const cardListN_temp: SwotCard[] = [];
      const cardListS_temp: SwotCard[] = [];
      const cardListW_temp: SwotCard[] = [];
      const cardListO_temp: SwotCard[] = [];
      const cardListT_temp: SwotCard[] = [];

      action.allCard.forEach(card => {
        if (card.containIn === 'N') {
          cardListN_temp.push(card);
        }
        if (card.containIn === 'S') {
          cardListS_temp.push(card);
        }
        if (card.containIn === 'W') {
          cardListW_temp.push(card);
        }
        if (card.containIn === 'O') {
          cardListO_temp.push(card);
        }
        if (card.containIn === 'T') {
          cardListT_temp.push(card);
        }
      });

      return {
        ...state,
        loading: false,
        cardListAll: action.allCard,
        cardListN: cardListN_temp,
        cardListS: cardListS_temp,
        cardListW: cardListW_temp,
        cardListO: cardListO_temp,
        cardListT: cardListT_temp
      };
    }

    default:
      return state;
  }
}

export function removeItem(itemArray, removeItemId) {
  return itemArray.filter(item => item.id !== removeItemId);
}

export function addItem(itemArray, addedItem, index) {
  itemArray.splice(index, 0, addedItem);
  return itemArray;
}

export function replaceItemWithId(itemArray, modifiedItem) {
  let itemIndex;

  // find index that match same id.
  for (let i = 0; i < itemArray.length; i++) {
    if (itemArray[i].id === modifiedItem.id) {
      itemIndex = i;
    }
  }

  itemArray.splice(itemIndex, 1, modifiedItem);
  return itemArray;
}
