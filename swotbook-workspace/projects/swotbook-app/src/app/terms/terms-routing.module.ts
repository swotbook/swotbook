import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsAndConditionsComponent} from './terms-and-conditions/terms-and-conditions.component';
import {DataProtectionComponent} from './data-protection/data-protection.component';

const routes: Routes = [

  { path: '', redirectTo: 'privacy-policy', pathMatch: 'full' },
  // { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'terms-and-conditions', component: TermsAndConditionsComponent },
  { path: 'data-protection', component: DataProtectionComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsRoutingModule { }
