// import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


import {CurrentVersion} from '../../projects/swotbook-app/src/app/app.version';

const functions = require('firebase-functions');
const omise = require('omise')({
  'secretKey': 'skey_5gyvldq98xl74qnzp8x',
});


const currentVersion = CurrentVersion;

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions


exports.createOmiseCustomer = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.customers.create({
    'description': `Firebase User ID: ${req.query.uid}`,
    'email': req.query.email


  }, (err:Error, customer:any) => {
    if (err) {
      //require by cloud functions
      console.log(err);
      res.json(err);
    } else {
      //Success
      res.json(customer)


    }
  });


});


exports.getCreditCards = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.customers.listCards(req.query.omiseId, (err:Error, cardList:any[]) => {
    if (err) {
      //require by cloud functions
      console.log(err);
      res.json(err);
    } else {
      //Success
      res.json(cardList);


    }
  });


});


exports.attachCreditCard = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.customers.update(
    req.query.omiseId,
    {card: `${req.query.cardToken}`}, (err:Error, customer:any) => {
      if (err) {
        //require by cloud functions
        console.log(err);
        res.json(err);
      } else {
        //Success
        res.json(customer);


      }
    });


});


exports.deleteCreditCard = functions.https.onRequest((req:any, res:any) => {


  // response.set('Access-Control-Allow-Origin', '*');
  // response.set('Access-Control-Allow-Methods', 'GET, POST');
  // response.set("Access-Control-Allow-Headers", "Content-Type");
  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.customers.destroyCard(
    req.query.omiseId,
    req.query.cardId, (err:Error, customer:any) => {
      if (err) {
        //require by cloud functions
        console.log(err);
        res.json(err);
      } else {
        //Success
        res.json(customer);


      }
    });


});


exports.scheduledCharge = functions.https.onRequest((req:any, res:any) => {


  const allowedOrigins = ['http://localhost:3779','https://www.swotbook.com'];
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
    res.header('Access-Control-Allow-Origin', origin); // restrict it to the required domain
  }


  res.header('Content-Type','application/json');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  let amount;
  let monthChargePeriod;
  let subscriptionType;
  const currency = req.query.currency;
  const isUSD = currency === 'usd';


  if (req.query.userSubType === 'prm') {


    if (req.query.period === 'monthly') {


      amount = isUSD? '299':'9900';
      monthChargePeriod = 1;
      subscriptionType = 'prm'


    } else if (req.query.period === 'annually') {


      amount = isUSD? '2990':'99000';
      monthChargePeriod = 12;
      subscriptionType = 'prm'


    }
  } else if (req.query.userSubType === 'pro') {
    if (req.query.period === 'monthly') {


      amount = isUSD? '499':'15900';
      monthChargePeriod = 1;
      subscriptionType = 'pro'




    } else if (req.query.period === 'annually') {


      amount = isUSD? '4990':'159000';
      monthChargePeriod = 12;
      subscriptionType = 'pro';


    }
  }




  const currentDate = new Date();
  const startDate = new Date(currentDate.setDate(currentDate.getDate() + 1));


  if (startDate.getDate() > 28) {
    startDate.setMonth(startDate.getMonth() + 1, 1);
  }


  const endDate = new Date(startDate);
  endDate.setFullYear(endDate.getFullYear() +10);


  let isoStartDate = startDate.toISOString();
  let isoEndDate = endDate.toISOString();
  isoStartDate = isoStartDate.slice(0, 10);
  isoEndDate = isoEndDate.slice(0, 10);


  const dateStr = isoStartDate.slice(8, 10);
  const date = parseInt(dateStr);


  omise.schedules.create({
    'every': monthChargePeriod,
    'period': 'month',
    'start_date': isoStartDate,
    'end_date': isoEndDate,
    'on': {
      'days_of_month': [date]
    },
    'charge': {
      'customer': req.query.omiseId,
      'card': req.query.cardId,
      'amount': amount,
      'currency': currency,
      'description': subscriptionType
    }
  }, (err:Error, schedule:any) => {
    if (err) {
      //require by cloud functions
      console.log(err);
      res.json(err);
    } else {
      //Success
      res.json(schedule);


    }
  });




});


exports.getScheduledCharge = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.schedules.retrieve(req.query.scheduleId, (err:Error, schedule:any) => {
    // omise.charges.schedules((err, schedules) => {
    if (err) {
      //require by cloud functions
      console.log(err);
      res.json(err);
    } else {
      //Success
      res.json(schedule);


    }
  });


});


exports.deleteScheduledCharge = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  omise.schedules.destroy(req.query.scheduleId, (err:Error, schedule:any) => {
    // omise.charges.schedules((err, schedules) => {
    if (err) {
      //require by cloud functions
      console.log(err);
      res.json(err);
    } else {
      //Success
      res.json(schedule);


    }
  });


});


exports.refundCharge = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  let refundAmount;
  const limitedFactor = 0.96; //omise will not refund 3.65% + 7% of amount charge.
  const dayLeft = parseInt(req.query.dayLeft);
  const period = req.query.period;
  // let subType = req.query.subType;
  // let currency = req.query.currency;
  const chargedAmount = parseInt(req.query.amount);


  if (period === 'monthly') {


    refundAmount = (chargedAmount*limitedFactor)*(dayLeft/30);


  } else if (period === 'annually') {


    refundAmount = (chargedAmount*limitedFactor)*(dayLeft/365);


  }


  // if (subType === 'prm') {
  //   if (period === 'monthly') {
  //
  //     refundAmount = (299*limitedFactor)*(dayLeft/30);
  //
  //   } else if (period === 'annually') {
  //
  //     refundAmount = (2990*limitedFactor)*(dayLeft/365);
  //
  //   }
  //
  // } else if (subType === 'pro') {
  //   if (period === 'monthly') {
  //
  //     refundAmount = (499*limitedFactor)*(dayLeft/30);
  //
  //
  //   } else if (period === 'annually') {
  //
  //     refundAmount = (4990*limitedFactor)*(dayLeft/365);
  //
  //   }
  // }


  omise.charges.createRefund(
    req.query.chargeId,{amount:refundAmount},
    (err:Error, schedule:any) => {
      // omise.charges.schedules((err, schedules) => {
      if (err) {
        //require by cloud functions
        res.json(err);
      } else {
        //Success
        res.json(schedule);


      }
    });


});


exports.getCurrentVersion = functions.https.onRequest((req:any, res:any) => {


  res.header('Content-Type', 'application/json');
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');


  res.json(currentVersion);


});



