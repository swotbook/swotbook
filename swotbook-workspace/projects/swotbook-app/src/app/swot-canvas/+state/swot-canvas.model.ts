import * as firebase from 'firebase/app';
import Timestamp = firebase.firestore.Timestamp;

export interface SwotCard {
  id?: string;
  title: string;
  index: number;
  containIn: 'S' | 'W' | 'O' | 'T' | 'N';
  prevContainIn: 'S' | 'W' | 'O' | 'T' | 'N';
  lastModified: 'MOVE' | 'INDEX' | 'DETAIL' | '';
  updatedAt: Timestamp;
  createdAt: Timestamp;
  createdBy: string; // user id
  canvasId: string; // canvas id
  score: number;
  weight: number;
  isChecked: boolean;
}

export interface SwotProject {
  id?: string;
  index: number;
  title: string;
  description: string;
  createdBy: string;
  createdAt: any;
  updatedAt: any;
  row1Name: string;
  row2Name: string;
  col1Name: string;
  col2Name: string;
  row1col1Name: string;
  row1col2Name: string;
  row2col1Name: string;
  row2col2Name: string;
  isShowLabel: boolean;
  isTodo: boolean;
  row1col1Color: string;
  row1col2Color: string;
  row2col1Color: string;
  row2col2Color: string;
  defaultHideTimeline: boolean;
}

export interface SwotCanvasModel {
  id?: string;
  inTimeline: string;
  createdBy: string;
  createdAt: any;
  updatedAt: any;
  title: string;
  analysis: string;
  allCard?: SwotCard[];
}

export interface SwotType {
  type: 'S' | 'W' | 'O' | 'T' | 'N';
}
