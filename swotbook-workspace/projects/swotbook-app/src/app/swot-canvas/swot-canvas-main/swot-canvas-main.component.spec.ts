import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwotCanvasMainComponent } from './swot-canvas-main.component';

describe('SwotCanvasMainComponent', () => {
  let component: SwotCanvasMainComponent;
  let fixture: ComponentFixture<SwotCanvasMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SwotCanvasMainComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwotCanvasMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
