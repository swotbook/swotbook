// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA4cLJQ_ifuHmb6TUZD-80wyVMRfXYXhCk',
    authDomain: 'www.swotbook.com',
    databaseURL: 'https://swotbook-e5e1b.firebaseio.com',
    projectId: 'swotbook-e5e1b',
    storageBucket: 'swotbook-e5e1b.appspot.com',
    messagingSenderId: '196097902008',
    appId: '1:196097902008:web:990ca014efcdcbd9547b4d'

  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
