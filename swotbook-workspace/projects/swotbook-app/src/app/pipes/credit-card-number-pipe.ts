import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'creditCardNumberPipe' })
export class CreditCardNumberPipe implements PipeTransform {
  constructor() {}

  transform(plainCreditCard: string): string {
    return plainCreditCard.replace(/\s+/g, '').replace(/(\d{4})/g, '$1 ').trim();
  }
}
