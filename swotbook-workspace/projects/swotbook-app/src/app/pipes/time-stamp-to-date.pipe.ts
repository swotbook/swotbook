import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as firebase from 'firebase/app';
import Timestamp = firebase.firestore.Timestamp;

@Pipe({ name: 'timeStampToDate' })
export class TimeStampToDatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {}

  transform(
    timestamp: Timestamp,
    format?: string,
    timezone?: string,
    locale?: string
  ): string {
    const date: Date = new Date(timestamp.toDate());
    return this.datePipe.transform(date, format, timezone, locale);
  }
}
