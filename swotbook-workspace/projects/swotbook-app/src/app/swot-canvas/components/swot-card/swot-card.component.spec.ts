import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwotCardComponent } from './swot-card.component';

describe('SwotCardComponent', () => {
  let component: SwotCardComponent;
  let fixture: ComponentFixture<SwotCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SwotCardComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwotCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
