import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SwotCanvasEffects } from './swot-canvas.effects';

describe('SwotCanvasService', () => {
  let actions$: Observable<any>;
  let effects: SwotCanvasEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwotCanvasEffects, provideMockActions(() => actions$)]
    });

    effects = TestBed.get(SwotCanvasEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
