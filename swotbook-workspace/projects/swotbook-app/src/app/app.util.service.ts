import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import Timestamp = firebase.firestore.Timestamp;

@Injectable({
  providedIn: 'root'
})
export class AppUtilService {
  constructor() {}

  getTimestampNow() {
    return firebase.firestore.Timestamp.now();
  }

  getDate(timeStamp: Timestamp) {
    const date: Date = new Date(timeStamp.toDate());
    return date;
  }

  detectMobile() {
    if ( navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    } else {
      return false;
    }
  }

  onlyNumberInput(event){
    const char = String.fromCharCode((event.which));

    if(!(/[0-9]/.test(char))){
      event.preventDefault();
    }
  }


}
