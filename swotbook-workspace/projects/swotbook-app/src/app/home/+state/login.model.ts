import * as Omise from 'omise';

export interface CurrentUser extends firebase.UserInfo {

}

export interface TimeStamp extends firebase.firestore.Timestamp{

}

export interface OmiseSchedule extends Omise.Schedules.ISchedule, Omise.IDestroyResponse {

}

export interface OmiseRefund extends Omise.Charges.IRefundResponse{

}

export interface AppUserInfo { // to be stroed in firebase
  omiseId?:string;
  activeChargeScheduleId:string;
  lastDateAvailable:string;
  lastOccurrence:string;
}

export interface OmiseCreditCard {
  name: string;
  number: string;
  expiration_month: string;
  expiration_year: string;
  security_code: number;
}

export interface SelectedPlan {
  isSelected?:boolean;
  subType:'std' | 'prm' | 'pro';
  period:'monthly'|'annually';
  currency:'thb'|'usd';

}
