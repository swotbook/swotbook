import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-features-section',
  templateUrl: './features-section.component.html',
  styleUrls: ['./features-section.component.scss']
})
export class FeaturesSectionComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  constructor() { }

  ngOnInit() {
  }

}
