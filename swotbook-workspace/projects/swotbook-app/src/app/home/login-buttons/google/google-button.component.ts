import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginState } from '../../+state/login.reducer';
import { GoogleLogin } from '../../+state/login.actions';

@Component({
  selector: 'app-google-button',
  templateUrl: './google-button.component.html',
  styleUrls: ['./google-button.component.scss']
})
export class GoogleButtonComponent implements OnInit {

  constructor(
    public store: Store<LoginState>

  ) {

  }

  ngOnInit() {
  }


  googleLogin() {
    this.store.dispatch(new GoogleLogin());
  }


}
