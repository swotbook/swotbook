import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomePageComponent} from './home-page/home-page.component';
import {StoreModule} from '@ngrx/store';
import {loginInitialState, loginReducer} from './+state/login.reducer';
import {EffectsModule} from '@ngrx/effects';
import {LoginEffects} from './+state/login.effects';
import {IntroSectionComponent} from './intro-section/intro-section.component';
import {FeaturesSectionComponent} from './features-section/features-section.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FooterSectionComponent} from './footer-section/footer-section.component';
import {CustomizeIdeaSectionComponent} from './customize-idea-section/customize-idea-section.component';
import {CtaSectionComponent} from './cta-section/cta-section.component';
import {PricingsSectionComponent} from './pricings-section/pricings-section.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TestimonialsSectionComponent} from './testimonials-section/testimonials-section.component';
import {PricingComponent} from './pricing/pricing.component';
import {HeaderSectionComponent} from './header-section/header-section.component';
import {GoogleButtonComponent} from './login-buttons/google/google-button.component';
import {FacebookButtonComponent} from './login-buttons/facebook-button/facebook-button.component';
import {WipPageComponent} from './wip-page/wip-page.component';
import {PaymentComponent} from './payment/payment.component';
import {CreditCardNumberPipe} from '../pipes/credit-card-number-pipe';
import {LoginPageComponent} from './login-page/login-page.component';
import {SubscriptionSuccessComponent} from './subscription-success/subscription-success.component';
import {LoginSignupDialogComponent} from './dialogs/login-signup-dialog/login-signup-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    StoreModule.forFeature('login', loginReducer, {
      initialState: loginInitialState
    }),
    EffectsModule.forFeature([LoginEffects]),
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    FlexLayoutModule,
    MatListModule,
    FormsModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDialogModule,
    MatMenuModule
  ],
  declarations: [
    HomePageComponent,
    IntroSectionComponent,
    FeaturesSectionComponent,
    FooterSectionComponent,
    CustomizeIdeaSectionComponent,
    CtaSectionComponent,
    PricingsSectionComponent,
    TestimonialsSectionComponent,
    PricingComponent,
    HeaderSectionComponent,
    GoogleButtonComponent,
    FacebookButtonComponent,
    WipPageComponent,
    PaymentComponent,
    CreditCardNumberPipe,
    LoginPageComponent,
    SubscriptionSuccessComponent,
    LoginSignupDialogComponent
  ],
  exports: [
    HeaderSectionComponent,
    FooterSectionComponent
  ],
  providers: [LoginEffects],
  entryComponents: [
    LoginSignupDialogComponent
  ]
})
export class HomeModule {
}
